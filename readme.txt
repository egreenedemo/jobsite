Peak Technical Form Mailer Documentation
-------------------------------------------------------------------------------

URL: http://www.peaktechnical.com/engineers/thankyou

Mail Configuration: /application/configs/application.ini

Variables: 

mail.useAuthentication
mail.username
mail.password
mail.sender
mail.thankyousubject
mail.thankyourecipient

HTML Templates:

/application/views/scripts/thankyou/index.phtml - Contains web page message: 
"Thank you for your interest in Peak Technical Staffing USA.
<br />
<br />
You will be contacted shortly to further discuss your needs."

/data/templates/email_thankyou.html - Contains email message body with variables that will be replaced with form content.
