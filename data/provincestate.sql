/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : peakjobs

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2012-08-02 01:42:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `provincestate`
-- ----------------------------
DROP TABLE IF EXISTS `provincestate`;
CREATE TABLE `provincestate` (
  `provinceStateID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countryID` int(10) unsigned NOT NULL,
  `state` varchar(50) DEFAULT NULL,
  `abbreviation` char(2) DEFAULT NULL,
  PRIMARY KEY (`provinceStateID`),
  KEY `fk_provinceState_country1` (`countryID`) USING BTREE,
  CONSTRAINT `provincestate_ibfk_1` FOREIGN KEY (`countryID`) REFERENCES `country` (`countryID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of provincestate
-- ----------------------------
INSERT INTO `provincestate` VALUES ('1', '2', 'Alaska', 'AK');
INSERT INTO `provincestate` VALUES ('2', '2', 'Alabama', 'AL');
INSERT INTO `provincestate` VALUES ('3', '2', 'Arizona', 'AZ');
INSERT INTO `provincestate` VALUES ('4', '2', 'Arkansas', 'AR');
INSERT INTO `provincestate` VALUES ('5', '2', 'California', 'CA');
INSERT INTO `provincestate` VALUES ('6', '2', 'Colorado', 'CO');
INSERT INTO `provincestate` VALUES ('7', '2', 'Connecticut', 'CT');
INSERT INTO `provincestate` VALUES ('8', '2', 'Delaware', 'DE');
INSERT INTO `provincestate` VALUES ('9', '2', 'District of Columbia', 'DC');
INSERT INTO `provincestate` VALUES ('10', '2', 'Florida', 'FL');
INSERT INTO `provincestate` VALUES ('11', '2', 'Georgia', 'GA');
INSERT INTO `provincestate` VALUES ('12', '2', 'Hawaii', 'HI');
INSERT INTO `provincestate` VALUES ('13', '2', 'Idaho', 'ID');
INSERT INTO `provincestate` VALUES ('14', '2', 'Illinois', 'IL');
INSERT INTO `provincestate` VALUES ('15', '2', 'Indiana', 'IN');
INSERT INTO `provincestate` VALUES ('16', '2', 'Iowa', 'IA');
INSERT INTO `provincestate` VALUES ('17', '2', 'Kansas', 'KS');
INSERT INTO `provincestate` VALUES ('18', '2', 'Kentucky', 'KY');
INSERT INTO `provincestate` VALUES ('19', '2', 'Louisiana', 'LA');
INSERT INTO `provincestate` VALUES ('20', '2', 'Maine', 'ME');
INSERT INTO `provincestate` VALUES ('21', '2', 'Maryland', 'MD');
INSERT INTO `provincestate` VALUES ('22', '2', 'Massachusetts', 'MA');
INSERT INTO `provincestate` VALUES ('23', '2', 'Michigan', 'MI');
INSERT INTO `provincestate` VALUES ('24', '2', 'Minnesota', 'MN');
INSERT INTO `provincestate` VALUES ('25', '2', 'Mississippi', 'MS');
INSERT INTO `provincestate` VALUES ('26', '2', 'Missouri', 'MO');
INSERT INTO `provincestate` VALUES ('27', '2', 'Montana', 'MT');
INSERT INTO `provincestate` VALUES ('28', '2', 'Nebraska', 'NE');
INSERT INTO `provincestate` VALUES ('29', '2', 'Nevada', 'NV');
INSERT INTO `provincestate` VALUES ('30', '2', 'New Hampshire', 'NH');
INSERT INTO `provincestate` VALUES ('31', '2', 'New Jersey', 'NJ');
INSERT INTO `provincestate` VALUES ('32', '2', 'New Mexico', 'NM');
INSERT INTO `provincestate` VALUES ('33', '2', 'New York', 'NY');
INSERT INTO `provincestate` VALUES ('34', '2', 'North Carolina', 'NC');
INSERT INTO `provincestate` VALUES ('35', '2', 'North Dakota', 'ND');
INSERT INTO `provincestate` VALUES ('36', '2', 'Ohio', 'OH');
INSERT INTO `provincestate` VALUES ('37', '2', 'Oklahoma', 'OK');
INSERT INTO `provincestate` VALUES ('38', '2', 'Oregon', 'OR');
INSERT INTO `provincestate` VALUES ('39', '2', 'Pennsylvania', 'PA');
INSERT INTO `provincestate` VALUES ('40', '2', 'Puerto Rico', 'PR');
INSERT INTO `provincestate` VALUES ('41', '2', 'Rhode Island', 'RI');
INSERT INTO `provincestate` VALUES ('42', '2', 'South Carolina', 'SC');
INSERT INTO `provincestate` VALUES ('43', '2', 'South Dakota', 'SD');
INSERT INTO `provincestate` VALUES ('44', '2', 'Tennessee', 'TN');
INSERT INTO `provincestate` VALUES ('45', '2', 'Texas', 'TX');
INSERT INTO `provincestate` VALUES ('46', '2', 'Utah', 'UT');
INSERT INTO `provincestate` VALUES ('47', '2', 'Vermont', 'VT');
INSERT INTO `provincestate` VALUES ('48', '2', 'Virginia', 'VA');
INSERT INTO `provincestate` VALUES ('49', '2', 'Washington', 'WA');
INSERT INTO `provincestate` VALUES ('50', '2', 'West Virginia', 'WV');
INSERT INTO `provincestate` VALUES ('51', '2', 'Wisconsin', 'WI');
INSERT INTO `provincestate` VALUES ('52', '2', 'Wyoming', 'WY');
