/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : peakjobs

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2012-08-02 01:38:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `candidates`
-- ----------------------------
DROP TABLE IF EXISTS `candidates`;
CREATE TABLE `candidates` (
  `candidateID` int(11) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `disciplineID` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(2) NOT NULL,
  `updated` date NOT NULL,
  `resume` text,
  PRIMARY KEY (`candidateID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of candidates
-- ----------------------------
INSERT INTO `candidates` VALUES ('1', 'JOHN', '1', 'PITTSBURGH', 'PA', '2006-04-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('4', 'KATHLEEN', '4', 'PITTSBURGH', 'PA', '2002-08-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('7', 'ROBIN', '7', 'PITTSBURGH', 'PA', '2009-03-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('8', 'MILANCA', '8', 'PITTSBURGH', 'PA', '2003-06-17', 'This will be the resume');
INSERT INTO `candidates` VALUES ('12', 'GEORGE', '1', 'PITTSBURGH', 'PA', '2003-08-07', 'This will be the resume');
INSERT INTO `candidates` VALUES ('18', 'BRUCE', '1', 'GREENSBURG', 'PA', '2003-03-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('19', 'DOUGLAS', '1', 'PITTSBURGH', 'PA', '2001-01-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('20', 'GARY', '2', 'W.BROWNSVILLE', 'PA', '2007-03-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('29', 'TIMOTHY', '2', 'PITTSBURGH', 'PA', '1998-12-05', 'This will be the resume');
INSERT INTO `candidates` VALUES ('30', 'JEANNINE', '3', 'MANSFIELD', 'OH', '1998-12-30', 'This will be the resume');
INSERT INTO `candidates` VALUES ('36', 'JOHN', '3', 'PITTSBURGH', 'PA', '2001-08-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('37', 'PETER', '3', 'ELLWOOD CITY', 'PA', '1999-01-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('38', 'MARC', '3', 'PITTSBURGH', 'PA', '2001-05-31', 'This will be the resume');
INSERT INTO `candidates` VALUES ('42', 'GERALD', '4', 'MONROEVILLE', 'PA', '2000-11-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('47', 'DAVID', '4', 'TRAFFORD', 'PA', '2002-08-27', 'This will be the resume');
INSERT INTO `candidates` VALUES ('49', 'RALPH', '4', 'CHESWICK', 'PA', '1998-11-06', 'This will be the resume');
INSERT INTO `candidates` VALUES ('51', 'JEFFREY', '5', 'GREENSBURG', 'PA', '1998-11-11', 'This will be the resume');
INSERT INTO `candidates` VALUES ('53', 'DAVID', '5', 'CORAOPOLIS', 'PA', '1998-10-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('54', 'EDWARD', '5', 'ALLISON PARK', 'PA', '2000-11-30', 'This will be the resume');
INSERT INTO `candidates` VALUES ('57', 'PRAVIN', '5', 'WEXFORD', 'PA', '1999-07-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('58', 'RICHARD', '5', 'CORAOPOLIS', 'PA', '2002-05-22', 'This will be the resume');
INSERT INTO `candidates` VALUES ('60', 'DAVID', '6', 'GLENSHAW', 'PA', '2003-10-15', 'This will be the resume');
INSERT INTO `candidates` VALUES ('64', 'DAVID', '6', 'PITTSBURGH', 'PA', '1998-10-29', 'This will be the resume');
INSERT INTO `candidates` VALUES ('68', 'ROBERT', '6', 'PITTSBURGH', 'PA', '2001-10-23', 'This will be the resume');
INSERT INTO `candidates` VALUES ('72', 'ALLEN', '7', 'VERONA', 'PA', '2001-04-23', 'This will be the resume');
INSERT INTO `candidates` VALUES ('75', 'STEVEN', '7', 'PITTSBURGH', 'PA', '1998-09-22', 'This will be the resume');
INSERT INTO `candidates` VALUES ('77', 'BENJAMIN', '7', 'SHIPPENVILLE', 'PA', '2001-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('92', 'FRANK', '9', 'CORAOPOLIS', 'PA', '2001-02-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('99', 'WILLIAM', '9', 'PITTSBURGH', 'PA', '2000-07-18', 'This will be the resume');
INSERT INTO `candidates` VALUES ('101', 'JAMES', '1', 'MONONGAHELA', 'PA', '2001-05-23', 'This will be the resume');
INSERT INTO `candidates` VALUES ('106', 'ROSE', '1', 'SAINT PETERSBURG', 'FL', '1999-02-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('107', 'TOM', '1', 'PITTSBURGH', 'PA', '2000-03-27', 'This will be the resume');
INSERT INTO `candidates` VALUES ('109', 'BRIAN', '1', 'PITTSBURGH', 'PA', '1998-12-30', 'This will be the resume');
INSERT INTO `candidates` VALUES ('110', 'WESLIE', '1', 'PITCAIRN', 'PA', '2001-04-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('117', 'WILLARD', '1', '309 DUFF ROAD', 'PA', '1998-10-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('118', 'CHARLES', '1', 'MURRYSVILLE', 'PA', '1999-02-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('120', 'MELISSA', '1', 'DUNCANSVILLE', 'PA', '2001-04-02', 'This will be the resume');
INSERT INTO `candidates` VALUES ('121', 'SHAWN', '1', 'ROCHESTER', 'PA', '1999-12-10', 'This will be the resume');
INSERT INTO `candidates` VALUES ('122', 'WILLIAM', '1', 'PITTSBURGH', 'PA', '2000-07-31', 'This will be the resume');
INSERT INTO `candidates` VALUES ('125', 'MICHAEL', '1', 'PITTSBURGH', 'PA', '2007-04-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('126', 'JOHN', '1', 'UPPER ST. CLAIR', 'PA', '1998-10-06', 'This will be the resume');
INSERT INTO `candidates` VALUES ('127', 'JACQUELYN', '1', 'NEW STANTON', 'PA', '2003-05-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('128', 'BRYAN', '1', 'IRWIN', 'PA', '2000-11-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('130', 'DANIEL', '1', 'STONEBORO', 'PA', '1999-03-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('133', 'JOHN', '1', 'MEDINA', 'OH', '1998-07-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('135', 'JAMES', '1', 'BENTLEYVILLE', 'PA', '1998-11-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('139', 'TIMOTHY', '1', 'VERONA', 'PA', '2003-10-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('142', 'KIRBY', '1', 'CARMICHAELS', 'PA', '2001-06-18', 'This will be the resume');
INSERT INTO `candidates` VALUES ('144', 'AUGUST', '1', 'MURRYSVILLE', 'PA', '2003-03-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('146', 'ROBERTA', '1', 'BRIDGEVILLE', 'PA', '2001-09-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('150', 'JOHN', '1', 'PITTSBURGH', 'PA', '2007-04-10', 'This will be the resume');
INSERT INTO `candidates` VALUES ('152', 'PAUL', '1', 'LIBRARY', 'PA', '2007-04-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('153', 'VINCENT', '1', 'ELLWOOD CITY', 'PA', '2001-12-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('155', 'JAMES', '1', 'BELLE VERNON', 'PA', '2002-08-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('156', 'ROBERT', '1', 'PITTSBURGH', 'PA', '2000-06-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('157', 'RICHARD', '1', 'GLENSHAW', 'PA', '2003-08-29', 'This will be the resume');
INSERT INTO `candidates` VALUES ('158', 'RONALD', '1', 'PITTSBURGH', 'PA', '2000-08-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('166', 'JOHN', '1', 'CRANBERRY TWP', 'PA', '2000-11-15', 'This will be the resume');
INSERT INTO `candidates` VALUES ('169', 'JOHN', '1', 'PITTSBURGH', 'PA', '2001-09-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('170', 'JEFFERY', '1', 'UNIONTOWN', 'PA', '2000-11-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('171', 'FRANK', '1', 'SPRUCE CREEK', 'PA', '2002-02-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('172', 'VEERA', '1', 'GAITHERSBURG', 'MD', '2008-03-05', 'This will be the resume');
INSERT INTO `candidates` VALUES ('173', 'RONALD', '1', 'TARENTUM', 'PA', '2000-08-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('180', 'WILLIAM', '1', 'EMMAUS', 'PA', '2001-11-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('185', 'CHRIS', '1', 'EAST PITTSBURGH', 'PA', '1999-07-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('186', 'JAY', '1', 'PITTSBURGH', 'PA', '2000-07-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('187', 'CHRISTOPHER', '1', 'CLEARFIELD', 'PA', '1998-11-06', 'This will be the resume');
INSERT INTO `candidates` VALUES ('194', 'JOHN', '1', 'NEW GALILEE', 'PA', '2001-02-05', 'This will be the resume');
INSERT INTO `candidates` VALUES ('195', 'STEVEN', '1', 'NEW BRIGHTON', 'PA', '2001-08-07', 'This will be the resume');
INSERT INTO `candidates` VALUES ('200', 'RICHARD', '2', 'JOHNSTOWN', 'PA', '2007-04-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('202', 'EDWARD', '2', 'PITTSBURGH', 'PA', '2001-10-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('209', 'JON', '2', 'PITTSBURGH', 'PA', '2000-07-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('211', 'RALPH', '2', 'W. PORTSMOUTH', 'OH', '1998-07-27', 'This will be the resume');
INSERT INTO `candidates` VALUES ('218', 'KEVIN', '2', 'MCKEESPORT', 'PA', '2001-06-15', 'This will be the resume');
INSERT INTO `candidates` VALUES ('227', 'RONALD', '2', 'PITTSBURGH', 'PA', '2008-05-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('229', 'ROGER', '2', 'PITTSBURGH', 'PA', '2003-04-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('230', 'RAMESH', '2', 'WALDORF', 'MD', '2006-05-30', 'This will be the resume');
INSERT INTO `candidates` VALUES ('237', 'DAVID', '2', 'FREEDOM', 'PA', '2000-04-11', 'This will be the resume');
INSERT INTO `candidates` VALUES ('238', 'ROBERT', '2', 'FOMBELL', 'PA', '1999-08-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('239', 'THOMAS', '2', 'TEMPLETON', 'PA', '1999-07-17', 'This will be the resume');
INSERT INTO `candidates` VALUES ('242', 'LU', '2', 'NO CITY', 'CA', '1998-11-11', 'This will be the resume');
INSERT INTO `candidates` VALUES ('247', 'JOSEPH', '2', 'WHITEHALL', 'PA', '2000-08-08', 'This will be the resume');
INSERT INTO `candidates` VALUES ('250', 'CHARLES', '2', 'BADEN', 'PA', '2002-02-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('251', 'LAURIE', '2', 'PITTSBURGH', 'PA', '2001-01-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('253', 'EDWARD', '2', 'PITTSBURGH', 'PA', '1999-02-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('266', 'KENNETH', '2', 'N HUNTINGDON', 'PA', '2000-01-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('268', 'STEVEN', '2', 'HERMITAGE', 'PA', '2001-10-12', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21650', 'A', '2', 'MCKEES ROCKS', 'PA', '2005-07-22', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21653', 'ROBERT', '2', 'MOON TOWNSHIP', 'PA', '2004-06-22', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21654', 'DAVID', '2', 'ALIQUIPPA', 'PA', '1999-07-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21655', 'JEFFREY', '2', 'BUTLER', 'PA', '2003-11-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21657', 'SEYED', '2', 'GLENSHAW', 'PA', '2003-12-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21659', 'LYNNE', '2', 'AMBRIDGE', 'PA', '2001-04-06', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25255', 'SANJAY', '2', 'PITTSBURGH', 'PA', '2003-12-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25256', 'DAVID', '2', 'LOWER BURRELL', 'PA', '2006-07-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25257', 'JEANNE', '2', 'WHITE OAK', 'PA', '2004-02-27', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25258', 'TODD', '2', 'PITTSBURGH', 'PA', '2000-03-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25259', 'DAVID', '2', 'GRAMPIAN', 'PA', '2003-12-15', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25260', 'MICHAEL', '2', 'CARNEGIE', 'PA', '2003-09-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25262', 'BERNARD', '2', 'NORTH VERSAILLES', 'PA', '2003-12-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325221', 'Aaron', '3', 'Fountain Valley', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325222', 'Pablo', '3', 'El Cajon', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325223', 'Mitchell', '3', 'Anaheim', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325224', 'Ngoc', '3', 'san diego', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325225', 'David', '3', 'Los Angeles', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325226', 'Ryan', '3', 'Arroyo Grande', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325227', 'Madhusudan', '3', 'Azusa', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325228', 'Patrice', '3', 'Aliso Viejo', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325229', 'Huy', '3', 'Westminster', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325230', 'Alex', '3', 'Oak Park', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325232', 'Harsha', '3', 'Aliso Viejo', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325233', 'krunal', '3', 'Los Angeles', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325234', 'GAREN', '3', 'PASADENA', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325235', 'Reza', '3', 'Sylmar', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325236', 'Sam', '3', 'Mission Hills', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325237', 'Robert', '3', 'Camarillo', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325238', 'Igor', '3', 'La Crescenta', 'CA', '2011-04-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325239', 'shawn', '3', 'TOLUCA LAKE', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325240', 'John', '3', 'Troy', 'OH', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325241', 'Sydney', '3', 'Corona', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325242', 'Yayi', '3', 'Tenafly', 'NJ', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325243', 'John', '3', 'New Knoxville', 'TN', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325244', 'Kurt', '3', 'Highlands Ranch', 'CO', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325245', 'Joe', '3', 'Ashippun', 'WI', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325246', 'Fardin', '3', 'San Ramon', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325247', 'Bud', '3', 'Madison Heights', 'MI', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325248', 'Maria', '3', 'Burlington', 'MA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325249', 'manuel', '3', 'visalia', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325250', 'Jeremy', '3', 'Singer Island', 'FL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325251', 'Terence', '3', 'Orlando', 'FL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325252', 'Martin', '3', 'Allentown', 'PA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325253', 'Carlos', '3', '', 'MA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325254', 'Jason', '3', 'Waxhaw', 'NC', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325255', 'Matthew', '3', '', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325256', 'Gregory', '3', 'Hobart', 'IN', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325257', 'Yoshiro', '3', 'Lowell', 'MA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325258', 'Shaomin', '3', 'Irvine', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325259', 'Claudio', '3', 'Des Plaines', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325260', 'Janak', '3', 'SAN JOSE', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325261', 'James', '3', 'Los Angeles', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325262', 'John', '3', 'Concord', 'TN', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325263', 'James', '3', 'Oxford', 'AL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325264', 'Karl', '3', 'Silver Creek', 'GA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325265', 'David', '3', 'Lutz', 'FL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325266', 'Viralkumar', '3', 'methuen', 'MA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325267', 'Kanchi', '3', 'Chino Hills', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325268', 'Kamal', '3', 'Schaumburg', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325269', 'Dina', '3', 'Hanover Park', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325270', 'Riad', '3', 'Bridgeton', 'MO', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325271', 'NEHA', '3', 'Crystal Lake', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325272', 'Jatin', '3', 'Tinton Falls', 'NJ', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325273', 'Shreyas', '3', 'Arlington', 'VA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325274', 'Ishwar', '3', 'st peters', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325275', 'Angelica', '3', 'Georgetown', 'TX', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325276', 'Deepen', '3', 'Lawrenceville', 'NJ', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325277', 'Sergio', '3', 'San Diego', 'CA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325278', 'Dominick', '3', 'Humble', 'TX', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325280', 'Christopher', '3', 'Grand Blanc', 'MI', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325281', 'ALEXANDER', '3', 'San Diego', 'CA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325282', 'Eric', '3', 'Washington', 'DC', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325283', 'George', '3', 'Orange', 'TX', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325285', 'Eric', '3', 'Valley Cottage', 'NY', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325286', 'Patrick', '3', 'Great Falls', 'VA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325287', 'Lacy', '3', 'North Huntingdon', 'PA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325288', 'Daniel', '3', 'Columbiana', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325289', 'Ryan', '3', 'Dublin', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325290', 'Josef', '3', 'Astoria', 'NY', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325291', 'Hind (Heidi)', '3', 'Cleveland', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325292', 'Sean', '3', 'Jamestown', 'RI', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325293', 'Ronald', '3', 'Manassas', 'VA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325294', 'Jose', '3', 'Miami', 'FL', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325295', 'Cris', '3', 'Cincinnati', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325296', 'Kirk', '3', 'Brooksville', 'FL', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325297', 'Leonard', '3', 'Avila Beach', 'CA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325298', 'Clayton', '3', 'Columbus', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325299', 'Nathan', '3', 'Peru', 'IN', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325300', 'Dale', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325301', 'Gary', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325302', 'Robert', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325303', 'Michael', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325304', 'Mark', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325305', 'YU', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325306', 'Mark', '3', 'Monroeville', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325307', 'SIDDIQUE', '3', 'Corona', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325308', 'Gayed', '3', 'Alhambra', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325309', 'James', '3', 'Rancho Cucamonga', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325311', 'Steve', '3', 'Torrance', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325312', 'John', '3', 'Los Angeles', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325313', 'Michael', '3', 'New Kensington', 'PA', '2012-05-02', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325314', 'Eugene', '3', 'Freedom', 'PA', '2012-05-02', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325316', 'Craig', '3', 'Santa Clara', 'CA', '2012-05-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325317', 'Karen', '3', 'Humble', 'TX', '2012-05-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325318', 'Sevko', '3', 'Columbia', 'MD', '2012-05-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325319', 'Peter', '3', 'Cleburne', 'TX', '2012-05-08', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325320', 'Mohammed', '3', 'Knoxville', 'TN', '2012-05-04', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325321', 'Patrick', '3', 'Ballwin', 'MO', '2012-05-08', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325322', 'Rafael', '3', 'Orangevale', 'CA', '2011-01-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325323', 'Alfredo', '3', 'Kabul', 'CA', '2012-05-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325324', 'Tad', '3', 'Pittsburgh', 'PA', '2012-05-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325325', 'Sandchez', '3', 'West Mifflin', 'PA', '2012-05-24', 'This will be the resume');
