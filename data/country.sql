/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : peakjobs

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2012-08-02 01:40:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `country`
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `countryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countryName` varchar(50) DEFAULT NULL,
  `isActive` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`countryID`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'United States', '1');
INSERT INTO `country` VALUES ('2', 'Canada', '0');
INSERT INTO `country` VALUES ('3', 'Mexico', '0');
INSERT INTO `country` VALUES ('4', 'Afghanistan', '0');
INSERT INTO `country` VALUES ('5', 'Albania', '0');
INSERT INTO `country` VALUES ('6', 'Algeria', '0');
INSERT INTO `country` VALUES ('7', 'Andorra', '0');
INSERT INTO `country` VALUES ('8', 'Angola', '0');
INSERT INTO `country` VALUES ('9', 'Anguilla', '0');
INSERT INTO `country` VALUES ('10', 'Antarctica', '0');
INSERT INTO `country` VALUES ('11', 'Antigua and Barbuda', '0');
INSERT INTO `country` VALUES ('12', 'Argentina', '0');
INSERT INTO `country` VALUES ('13', 'Armenia', '0');
INSERT INTO `country` VALUES ('14', 'Aruba', '0');
INSERT INTO `country` VALUES ('15', 'Ascension Island', '0');
INSERT INTO `country` VALUES ('16', 'Australia', '0');
INSERT INTO `country` VALUES ('17', 'Austria', '0');
INSERT INTO `country` VALUES ('18', 'Azerbaijan', '0');
INSERT INTO `country` VALUES ('19', 'Bahamas', '0');
INSERT INTO `country` VALUES ('20', 'Bahrain', '0');
INSERT INTO `country` VALUES ('21', 'Bangladesh', '0');
INSERT INTO `country` VALUES ('22', 'Barbados', '0');
INSERT INTO `country` VALUES ('23', 'Belarus', '0');
INSERT INTO `country` VALUES ('24', 'Belgium', '0');
INSERT INTO `country` VALUES ('25', 'Belize', '0');
INSERT INTO `country` VALUES ('26', 'Benin', '0');
INSERT INTO `country` VALUES ('27', 'Bermuda', '0');
INSERT INTO `country` VALUES ('28', 'Bhutan', '0');
INSERT INTO `country` VALUES ('29', 'Bolivia', '0');
INSERT INTO `country` VALUES ('30', 'Bophuthatswana', '0');
INSERT INTO `country` VALUES ('31', 'Bosnia-Herzegovina', '0');
INSERT INTO `country` VALUES ('32', 'Botswana', '0');
INSERT INTO `country` VALUES ('33', 'Bouvet Island', '0');
INSERT INTO `country` VALUES ('34', 'Brazil', '0');
INSERT INTO `country` VALUES ('35', 'British Indian Ocean', '0');
INSERT INTO `country` VALUES ('36', 'British Virgin Islands', '0');
INSERT INTO `country` VALUES ('37', 'Brunei Darussalam', '0');
INSERT INTO `country` VALUES ('38', 'Bulgaria', '0');
INSERT INTO `country` VALUES ('39', 'Burkina Faso', '0');
INSERT INTO `country` VALUES ('40', 'Burundi', '0');
INSERT INTO `country` VALUES ('41', 'Cambodia', '0');
INSERT INTO `country` VALUES ('42', 'Cameroon', '0');
INSERT INTO `country` VALUES ('44', 'Cape Verde Island', '0');
INSERT INTO `country` VALUES ('45', 'Cayman Islands', '0');
INSERT INTO `country` VALUES ('46', 'Central Africa', '0');
INSERT INTO `country` VALUES ('47', 'Chad', '0');
INSERT INTO `country` VALUES ('48', 'Channel Islands', '0');
INSERT INTO `country` VALUES ('49', 'Chile', '0');
INSERT INTO `country` VALUES ('50', 'China, Peoples Republic', '0');
INSERT INTO `country` VALUES ('51', 'Christmas Island', '0');
INSERT INTO `country` VALUES ('52', 'Cocos (Keeling) Islands', '0');
INSERT INTO `country` VALUES ('53', 'Colombia', '0');
INSERT INTO `country` VALUES ('54', 'Comoros Islands', '0');
INSERT INTO `country` VALUES ('55', 'Congo', '0');
INSERT INTO `country` VALUES ('56', 'Cook Islands', '0');
INSERT INTO `country` VALUES ('57', 'Costa Rica', '0');
INSERT INTO `country` VALUES ('58', 'Croatia', '0');
INSERT INTO `country` VALUES ('59', 'Cuba', '0');
INSERT INTO `country` VALUES ('60', 'Cyprus', '0');
INSERT INTO `country` VALUES ('61', 'Czech Republic', '0');
INSERT INTO `country` VALUES ('62', 'Denmark', '0');
INSERT INTO `country` VALUES ('63', 'Djibouti', '0');
INSERT INTO `country` VALUES ('64', 'Dominica', '0');
INSERT INTO `country` VALUES ('65', 'Dominican Republic', '0');
INSERT INTO `country` VALUES ('66', 'Easter Island', '0');
INSERT INTO `country` VALUES ('67', 'Ecuador', '0');
INSERT INTO `country` VALUES ('68', 'Egypt', '0');
INSERT INTO `country` VALUES ('69', 'El Salvador', '0');
INSERT INTO `country` VALUES ('70', 'England', '0');
INSERT INTO `country` VALUES ('71', 'Equatorial Guinea', '0');
INSERT INTO `country` VALUES ('72', 'Estonia', '0');
INSERT INTO `country` VALUES ('73', 'Ethiopia', '0');
INSERT INTO `country` VALUES ('74', 'Falkland Islands', '0');
INSERT INTO `country` VALUES ('75', 'Faeroe Islands', '0');
INSERT INTO `country` VALUES ('76', 'Fiji', '0');
INSERT INTO `country` VALUES ('77', 'Finland', '0');
INSERT INTO `country` VALUES ('78', 'France', '0');
INSERT INTO `country` VALUES ('79', 'French Guyana', '0');
INSERT INTO `country` VALUES ('80', 'French Polynesia', '0');
INSERT INTO `country` VALUES ('81', 'Gabon', '0');
INSERT INTO `country` VALUES ('82', 'Gambia', '0');
INSERT INTO `country` VALUES ('83', 'Georgia Republic', '0');
INSERT INTO `country` VALUES ('84', 'Germany', '0');
INSERT INTO `country` VALUES ('85', 'Gibraltar', '0');
INSERT INTO `country` VALUES ('86', 'Greece', '0');
INSERT INTO `country` VALUES ('87', 'Greenland', '0');
INSERT INTO `country` VALUES ('88', 'Grenada', '0');
INSERT INTO `country` VALUES ('89', 'Guadeloupe (French)', '0');
INSERT INTO `country` VALUES ('90', 'Guatemala', '0');
INSERT INTO `country` VALUES ('91', 'Guernsey Island', '0');
INSERT INTO `country` VALUES ('92', 'Guinea Bissau', '0');
INSERT INTO `country` VALUES ('93', 'Guinea', '0');
INSERT INTO `country` VALUES ('94', 'Guyana', '0');
INSERT INTO `country` VALUES ('95', 'Haiti', '0');
INSERT INTO `country` VALUES ('96', 'Heard and McDonald Isls', '0');
INSERT INTO `country` VALUES ('97', 'Honduras', '0');
INSERT INTO `country` VALUES ('98', 'Hong Kong', '0');
INSERT INTO `country` VALUES ('99', 'Hungary', '0');
INSERT INTO `country` VALUES ('100', 'Iceland', '0');
INSERT INTO `country` VALUES ('101', 'India', '0');
INSERT INTO `country` VALUES ('102', 'Iran', '0');
INSERT INTO `country` VALUES ('103', 'Iraq', '0');
INSERT INTO `country` VALUES ('104', 'Ireland', '0');
INSERT INTO `country` VALUES ('105', 'Isle of Man', '0');
INSERT INTO `country` VALUES ('106', 'Israel', '0');
INSERT INTO `country` VALUES ('107', 'Italy', '0');
INSERT INTO `country` VALUES ('108', 'Ivory Coast', '0');
INSERT INTO `country` VALUES ('109', 'Jamaica', '0');
INSERT INTO `country` VALUES ('110', 'Japan', '0');
INSERT INTO `country` VALUES ('111', 'Jersey Island', '0');
INSERT INTO `country` VALUES ('112', 'Jordan', '0');
INSERT INTO `country` VALUES ('113', 'Kazakhstan', '0');
INSERT INTO `country` VALUES ('114', 'Kenya', '0');
INSERT INTO `country` VALUES ('115', 'Kiribati', '0');
INSERT INTO `country` VALUES ('116', 'Kuwait', '0');
INSERT INTO `country` VALUES ('117', 'Laos', '0');
INSERT INTO `country` VALUES ('118', 'Latvia', '0');
INSERT INTO `country` VALUES ('119', 'Lebanon', '0');
INSERT INTO `country` VALUES ('120', 'Lesotho', '0');
INSERT INTO `country` VALUES ('121', 'Liberia', '0');
INSERT INTO `country` VALUES ('122', 'Libya', '0');
INSERT INTO `country` VALUES ('123', 'Liechtenstein', '0');
INSERT INTO `country` VALUES ('124', 'Lithuania', '0');
INSERT INTO `country` VALUES ('125', 'Luxembourg', '0');
INSERT INTO `country` VALUES ('126', 'Macao', '0');
INSERT INTO `country` VALUES ('127', 'Macedonia', '0');
INSERT INTO `country` VALUES ('128', 'Madagascar', '0');
INSERT INTO `country` VALUES ('129', 'Malawi', '0');
INSERT INTO `country` VALUES ('130', 'Malaysia', '0');
INSERT INTO `country` VALUES ('131', 'Maldives', '0');
INSERT INTO `country` VALUES ('132', 'Mali', '0');
INSERT INTO `country` VALUES ('133', 'Malta', '0');
INSERT INTO `country` VALUES ('134', 'Martinique (French)', '0');
INSERT INTO `country` VALUES ('135', 'Mauritania', '0');
INSERT INTO `country` VALUES ('136', 'Mauritius', '0');
INSERT INTO `country` VALUES ('137', 'Mayotte', '0');
INSERT INTO `country` VALUES ('139', 'Micronesia', '0');
INSERT INTO `country` VALUES ('140', 'Moldavia', '0');
INSERT INTO `country` VALUES ('141', 'Monaco', '0');
INSERT INTO `country` VALUES ('142', 'Mongolia', '0');
INSERT INTO `country` VALUES ('143', 'Montenegro', '0');
INSERT INTO `country` VALUES ('144', 'Montserrat', '0');
INSERT INTO `country` VALUES ('145', 'Morocco', '0');
INSERT INTO `country` VALUES ('146', 'Mozambique', '0');
INSERT INTO `country` VALUES ('147', 'Myanmar', '0');
INSERT INTO `country` VALUES ('148', 'Namibia', '0');
INSERT INTO `country` VALUES ('149', 'Nauru', '0');
INSERT INTO `country` VALUES ('150', 'Nepal', '0');
INSERT INTO `country` VALUES ('151', 'Netherlands Antilles', '0');
INSERT INTO `country` VALUES ('152', 'Netherlands', '0');
INSERT INTO `country` VALUES ('153', 'New Caledonia (French)', '0');
INSERT INTO `country` VALUES ('154', 'New Zealand', '0');
INSERT INTO `country` VALUES ('155', 'Nicaragua', '0');
INSERT INTO `country` VALUES ('156', 'Niger', '0');
INSERT INTO `country` VALUES ('157', 'Niue', '0');
INSERT INTO `country` VALUES ('158', 'Norfolk Island', '0');
INSERT INTO `country` VALUES ('159', 'North Korea', '0');
INSERT INTO `country` VALUES ('160', 'Northern Ireland', '0');
INSERT INTO `country` VALUES ('161', 'Norway', '0');
INSERT INTO `country` VALUES ('162', 'Oman', '0');
INSERT INTO `country` VALUES ('163', 'Pakistan', '0');
INSERT INTO `country` VALUES ('164', 'Panama', '0');
INSERT INTO `country` VALUES ('165', 'Papua New Guinea', '0');
INSERT INTO `country` VALUES ('166', 'Paraguay', '0');
INSERT INTO `country` VALUES ('167', 'Peru', '0');
INSERT INTO `country` VALUES ('168', 'Philippines', '0');
INSERT INTO `country` VALUES ('169', 'Pitcairn Island', '0');
INSERT INTO `country` VALUES ('170', 'Poland', '0');
INSERT INTO `country` VALUES ('171', 'Polynesia (French)', '0');
INSERT INTO `country` VALUES ('172', 'Portugal', '0');
INSERT INTO `country` VALUES ('173', 'Qatar', '0');
INSERT INTO `country` VALUES ('174', 'Reunion Island', '0');
INSERT INTO `country` VALUES ('175', 'Romania', '0');
INSERT INTO `country` VALUES ('176', 'Russia', '0');
INSERT INTO `country` VALUES ('177', 'Rwanda', '0');
INSERT INTO `country` VALUES ('178', 'S.Georgia Sandwich Isls', '0');
INSERT INTO `country` VALUES ('179', 'Sao Tome, Principe', '0');
INSERT INTO `country` VALUES ('180', 'San Marino', '0');
INSERT INTO `country` VALUES ('181', 'Saudi Arabia', '0');
INSERT INTO `country` VALUES ('182', 'Scotland', '0');
INSERT INTO `country` VALUES ('183', 'Senegal', '0');
INSERT INTO `country` VALUES ('184', 'Serbia', '0');
INSERT INTO `country` VALUES ('185', 'Seychelles', '0');
INSERT INTO `country` VALUES ('186', 'Shetland', '0');
INSERT INTO `country` VALUES ('187', 'Sierra Leone', '0');
INSERT INTO `country` VALUES ('188', 'Singapore', '0');
INSERT INTO `country` VALUES ('189', 'Slovak Republic', '0');
INSERT INTO `country` VALUES ('190', 'Slovenia', '0');
INSERT INTO `country` VALUES ('191', 'Solomon Islands', '0');
INSERT INTO `country` VALUES ('192', 'Somalia', '0');
INSERT INTO `country` VALUES ('193', 'South Africa', '0');
INSERT INTO `country` VALUES ('194', 'South Korea', '0');
INSERT INTO `country` VALUES ('195', 'Spain', '0');
INSERT INTO `country` VALUES ('196', 'Sri Lanka', '0');
INSERT INTO `country` VALUES ('197', 'St. Helena', '0');
INSERT INTO `country` VALUES ('198', 'St. Lucia', '0');
INSERT INTO `country` VALUES ('199', 'St. Pierre Miquelon', '0');
INSERT INTO `country` VALUES ('200', 'St. Martins', '0');
INSERT INTO `country` VALUES ('201', 'St. Kitts Nevis Anguilla', '0');
INSERT INTO `country` VALUES ('202', 'St. Vincent Grenadines', '0');
INSERT INTO `country` VALUES ('203', 'Sudan', '0');
INSERT INTO `country` VALUES ('204', 'Suriname', '0');
INSERT INTO `country` VALUES ('205', 'Svalbard Jan Mayen', '0');
INSERT INTO `country` VALUES ('206', 'Swaziland', '0');
INSERT INTO `country` VALUES ('207', 'Sweden', '0');
INSERT INTO `country` VALUES ('208', 'Switzerland', '0');
INSERT INTO `country` VALUES ('209', 'Syria', '0');
INSERT INTO `country` VALUES ('210', 'Tajikistan', '0');
INSERT INTO `country` VALUES ('211', 'Taiwan', '0');
INSERT INTO `country` VALUES ('212', 'Tahiti', '0');
INSERT INTO `country` VALUES ('213', 'Tanzania', '0');
INSERT INTO `country` VALUES ('214', 'Thailand', '0');
INSERT INTO `country` VALUES ('215', 'Togo', '0');
INSERT INTO `country` VALUES ('216', 'Tokelau', '0');
INSERT INTO `country` VALUES ('217', 'Tonga', '0');
INSERT INTO `country` VALUES ('218', 'Trinidad and Tobago', '0');
INSERT INTO `country` VALUES ('219', 'Tunisia', '0');
INSERT INTO `country` VALUES ('220', 'Turkmenistan', '0');
INSERT INTO `country` VALUES ('221', 'Turks and Caicos Isls', '0');
INSERT INTO `country` VALUES ('222', 'Tuvalu', '0');
INSERT INTO `country` VALUES ('223', 'Uganda', '0');
INSERT INTO `country` VALUES ('224', 'Ukraine', '0');
INSERT INTO `country` VALUES ('225', 'United Arab Emirates', '0');
INSERT INTO `country` VALUES ('226', 'Uruguay', '0');
INSERT INTO `country` VALUES ('227', 'Uzbekistan', '0');
INSERT INTO `country` VALUES ('228', 'Vanuatu', '0');
INSERT INTO `country` VALUES ('229', 'Vatican City State', '0');
INSERT INTO `country` VALUES ('230', 'Venezuela', '0');
INSERT INTO `country` VALUES ('231', 'Vietnam', '0');
INSERT INTO `country` VALUES ('232', 'Virgin Islands (Brit)', '0');
INSERT INTO `country` VALUES ('233', 'Wales', '0');
INSERT INTO `country` VALUES ('234', 'Wallis Futuna Islands', '0');
INSERT INTO `country` VALUES ('235', 'Western Sahara', '0');
INSERT INTO `country` VALUES ('236', 'Western Samoa', '0');
INSERT INTO `country` VALUES ('237', 'Yemen', '0');
INSERT INTO `country` VALUES ('238', 'Yugoslavia', '0');
INSERT INTO `country` VALUES ('239', 'Zaire', '0');
INSERT INTO `country` VALUES ('240', 'Zambia', '0');
INSERT INTO `country` VALUES ('241', 'Zimbabwe', '0');
