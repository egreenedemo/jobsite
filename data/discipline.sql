/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50516
Source Host           : localhost:3306
Source Database       : peakjobs

Target Server Type    : MYSQL
Target Server Version : 50516
File Encoding         : 65001

Date: 2012-08-02 01:39:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `discipline`
-- ----------------------------
DROP TABLE IF EXISTS `discipline`;
CREATE TABLE `discipline` (
  `disciplineID` int(11) NOT NULL AUTO_INCREMENT,
  `discipline` varchar(100) NOT NULL,
  PRIMARY KEY (`disciplineID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of discipline
-- ----------------------------
INSERT INTO `discipline` VALUES ('1', 'First Discipline');
INSERT INTO `discipline` VALUES ('2', 'Second Discipline');
INSERT INTO `discipline` VALUES ('3', 'Third Discipline');
INSERT INTO `discipline` VALUES ('4', 'Fourth Discipline');
INSERT INTO `discipline` VALUES ('5', 'Fifth Discipline');
INSERT INTO `discipline` VALUES ('6', 'Sixth Discipline');
INSERT INTO `discipline` VALUES ('7', 'Seventh Discipline');
INSERT INTO `discipline` VALUES ('8', 'Eighth Discipline');
INSERT INTO `discipline` VALUES ('9', 'Ninth Discipline');
INSERT INTO `discipline` VALUES ('10', 'Tenth Discipline');
INSERT INTO `discipline` VALUES ('11', 'Eleventh Discipline');
INSERT INTO `discipline` VALUES ('12', 'Twelveth Discipline');
INSERT INTO `discipline` VALUES ('13', 'Thirteenth Discipline');
INSERT INTO `discipline` VALUES ('14', 'Fourteenth Discipline');
INSERT INTO `discipline` VALUES ('15', 'Fifteenth Discipline');
INSERT INTO `discipline` VALUES ('16', 'Sixteenth Discipline');
INSERT INTO `discipline` VALUES ('17', 'Seventeenth Discipline');
INSERT INTO `discipline` VALUES ('18', 'Eighteenth Discipline');
