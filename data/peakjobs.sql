/*
Navicat MySQL Data Transfer

Source Server         : eljon.bitnamiapp.com
Source Server Version : 50156
Source Host           : localhost:3306
Source Database       : peakjobs

Target Server Type    : MYSQL
Target Server Version : 50156
File Encoding         : 65001

Date: 2012-08-02 01:48:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `candidates`
-- ----------------------------
DROP TABLE IF EXISTS `candidates`;
CREATE TABLE `candidates` (
  `candidateID` int(11) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `disciplineID` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(2) NOT NULL,
  `updated` date NOT NULL,
  `resume` text,
  PRIMARY KEY (`candidateID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of candidates
-- ----------------------------
INSERT INTO `candidates` VALUES ('1', 'JOHN', '1', 'PITTSBURGH', 'PA', '2006-04-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('4', 'KATHLEEN', '4', 'PITTSBURGH', 'PA', '2002-08-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('7', 'ROBIN', '7', 'PITTSBURGH', 'PA', '2009-03-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('8', 'MILANCA', '8', 'PITTSBURGH', 'PA', '2003-06-17', 'This will be the resume');
INSERT INTO `candidates` VALUES ('12', 'GEORGE', '1', 'PITTSBURGH', 'PA', '2003-08-07', 'This will be the resume');
INSERT INTO `candidates` VALUES ('18', 'BRUCE', '1', 'GREENSBURG', 'PA', '2003-03-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('19', 'DOUGLAS', '1', 'PITTSBURGH', 'PA', '2001-01-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('20', 'GARY', '2', 'W.BROWNSVILLE', 'PA', '2007-03-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('29', 'TIMOTHY', '2', 'PITTSBURGH', 'PA', '1998-12-05', 'This will be the resume');
INSERT INTO `candidates` VALUES ('30', 'JEANNINE', '3', 'MANSFIELD', 'OH', '1998-12-30', 'This will be the resume');
INSERT INTO `candidates` VALUES ('36', 'JOHN', '3', 'PITTSBURGH', 'PA', '2001-08-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('37', 'PETER', '3', 'ELLWOOD CITY', 'PA', '1999-01-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('38', 'MARC', '3', 'PITTSBURGH', 'PA', '2001-05-31', 'This will be the resume');
INSERT INTO `candidates` VALUES ('42', 'GERALD', '4', 'MONROEVILLE', 'PA', '2000-11-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('47', 'DAVID', '4', 'TRAFFORD', 'PA', '2002-08-27', 'This will be the resume');
INSERT INTO `candidates` VALUES ('49', 'RALPH', '4', 'CHESWICK', 'PA', '1998-11-06', 'This will be the resume');
INSERT INTO `candidates` VALUES ('51', 'JEFFREY', '5', 'GREENSBURG', 'PA', '1998-11-11', 'This will be the resume');
INSERT INTO `candidates` VALUES ('53', 'DAVID', '5', 'CORAOPOLIS', 'PA', '1998-10-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('54', 'EDWARD', '5', 'ALLISON PARK', 'PA', '2000-11-30', 'This will be the resume');
INSERT INTO `candidates` VALUES ('57', 'PRAVIN', '5', 'WEXFORD', 'PA', '1999-07-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('58', 'RICHARD', '5', 'CORAOPOLIS', 'PA', '2002-05-22', 'This will be the resume');
INSERT INTO `candidates` VALUES ('60', 'DAVID', '6', 'GLENSHAW', 'PA', '2003-10-15', 'This will be the resume');
INSERT INTO `candidates` VALUES ('64', 'DAVID', '6', 'PITTSBURGH', 'PA', '1998-10-29', 'This will be the resume');
INSERT INTO `candidates` VALUES ('68', 'ROBERT', '6', 'PITTSBURGH', 'PA', '2001-10-23', 'This will be the resume');
INSERT INTO `candidates` VALUES ('72', 'ALLEN', '7', 'VERONA', 'PA', '2001-04-23', 'This will be the resume');
INSERT INTO `candidates` VALUES ('75', 'STEVEN', '7', 'PITTSBURGH', 'PA', '1998-09-22', 'This will be the resume');
INSERT INTO `candidates` VALUES ('77', 'BENJAMIN', '7', 'SHIPPENVILLE', 'PA', '2001-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('92', 'FRANK', '9', 'CORAOPOLIS', 'PA', '2001-02-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('99', 'WILLIAM', '9', 'PITTSBURGH', 'PA', '2000-07-18', 'This will be the resume');
INSERT INTO `candidates` VALUES ('101', 'JAMES', '1', 'MONONGAHELA', 'PA', '2001-05-23', 'This will be the resume');
INSERT INTO `candidates` VALUES ('106', 'ROSE', '1', 'SAINT PETERSBURG', 'FL', '1999-02-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('107', 'TOM', '1', 'PITTSBURGH', 'PA', '2000-03-27', 'This will be the resume');
INSERT INTO `candidates` VALUES ('109', 'BRIAN', '1', 'PITTSBURGH', 'PA', '1998-12-30', 'This will be the resume');
INSERT INTO `candidates` VALUES ('110', 'WESLIE', '1', 'PITCAIRN', 'PA', '2001-04-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('117', 'WILLARD', '1', '309 DUFF ROAD', 'PA', '1998-10-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('118', 'CHARLES', '1', 'MURRYSVILLE', 'PA', '1999-02-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('120', 'MELISSA', '1', 'DUNCANSVILLE', 'PA', '2001-04-02', 'This will be the resume');
INSERT INTO `candidates` VALUES ('121', 'SHAWN', '1', 'ROCHESTER', 'PA', '1999-12-10', 'This will be the resume');
INSERT INTO `candidates` VALUES ('122', 'WILLIAM', '1', 'PITTSBURGH', 'PA', '2000-07-31', 'This will be the resume');
INSERT INTO `candidates` VALUES ('125', 'MICHAEL', '1', 'PITTSBURGH', 'PA', '2007-04-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('126', 'JOHN', '1', 'UPPER ST. CLAIR', 'PA', '1998-10-06', 'This will be the resume');
INSERT INTO `candidates` VALUES ('127', 'JACQUELYN', '1', 'NEW STANTON', 'PA', '2003-05-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('128', 'BRYAN', '1', 'IRWIN', 'PA', '2000-11-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('130', 'DANIEL', '1', 'STONEBORO', 'PA', '1999-03-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('133', 'JOHN', '1', 'MEDINA', 'OH', '1998-07-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('135', 'JAMES', '1', 'BENTLEYVILLE', 'PA', '1998-11-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('139', 'TIMOTHY', '1', 'VERONA', 'PA', '2003-10-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('142', 'KIRBY', '1', 'CARMICHAELS', 'PA', '2001-06-18', 'This will be the resume');
INSERT INTO `candidates` VALUES ('144', 'AUGUST', '1', 'MURRYSVILLE', 'PA', '2003-03-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('146', 'ROBERTA', '1', 'BRIDGEVILLE', 'PA', '2001-09-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('150', 'JOHN', '1', 'PITTSBURGH', 'PA', '2007-04-10', 'This will be the resume');
INSERT INTO `candidates` VALUES ('152', 'PAUL', '1', 'LIBRARY', 'PA', '2007-04-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('153', 'VINCENT', '1', 'ELLWOOD CITY', 'PA', '2001-12-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('155', 'JAMES', '1', 'BELLE VERNON', 'PA', '2002-08-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('156', 'ROBERT', '1', 'PITTSBURGH', 'PA', '2000-06-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('157', 'RICHARD', '1', 'GLENSHAW', 'PA', '2003-08-29', 'This will be the resume');
INSERT INTO `candidates` VALUES ('158', 'RONALD', '1', 'PITTSBURGH', 'PA', '2000-08-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('166', 'JOHN', '1', 'CRANBERRY TWP', 'PA', '2000-11-15', 'This will be the resume');
INSERT INTO `candidates` VALUES ('169', 'JOHN', '1', 'PITTSBURGH', 'PA', '2001-09-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('170', 'JEFFERY', '1', 'UNIONTOWN', 'PA', '2000-11-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('171', 'FRANK', '1', 'SPRUCE CREEK', 'PA', '2002-02-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('172', 'VEERA', '1', 'GAITHERSBURG', 'MD', '2008-03-05', 'This will be the resume');
INSERT INTO `candidates` VALUES ('173', 'RONALD', '1', 'TARENTUM', 'PA', '2000-08-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('180', 'WILLIAM', '1', 'EMMAUS', 'PA', '2001-11-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('185', 'CHRIS', '1', 'EAST PITTSBURGH', 'PA', '1999-07-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('186', 'JAY', '1', 'PITTSBURGH', 'PA', '2000-07-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('187', 'CHRISTOPHER', '1', 'CLEARFIELD', 'PA', '1998-11-06', 'This will be the resume');
INSERT INTO `candidates` VALUES ('194', 'JOHN', '1', 'NEW GALILEE', 'PA', '2001-02-05', 'This will be the resume');
INSERT INTO `candidates` VALUES ('195', 'STEVEN', '1', 'NEW BRIGHTON', 'PA', '2001-08-07', 'This will be the resume');
INSERT INTO `candidates` VALUES ('200', 'RICHARD', '2', 'JOHNSTOWN', 'PA', '2007-04-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('202', 'EDWARD', '2', 'PITTSBURGH', 'PA', '2001-10-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('209', 'JON', '2', 'PITTSBURGH', 'PA', '2000-07-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('211', 'RALPH', '2', 'W. PORTSMOUTH', 'OH', '1998-07-27', 'This will be the resume');
INSERT INTO `candidates` VALUES ('218', 'KEVIN', '2', 'MCKEESPORT', 'PA', '2001-06-15', 'This will be the resume');
INSERT INTO `candidates` VALUES ('227', 'RONALD', '2', 'PITTSBURGH', 'PA', '2008-05-14', 'This will be the resume');
INSERT INTO `candidates` VALUES ('229', 'ROGER', '2', 'PITTSBURGH', 'PA', '2003-04-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('230', 'RAMESH', '2', 'WALDORF', 'MD', '2006-05-30', 'This will be the resume');
INSERT INTO `candidates` VALUES ('237', 'DAVID', '2', 'FREEDOM', 'PA', '2000-04-11', 'This will be the resume');
INSERT INTO `candidates` VALUES ('238', 'ROBERT', '2', 'FOMBELL', 'PA', '1999-08-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('239', 'THOMAS', '2', 'TEMPLETON', 'PA', '1999-07-17', 'This will be the resume');
INSERT INTO `candidates` VALUES ('242', 'LU', '2', 'NO CITY', 'CA', '1998-11-11', 'This will be the resume');
INSERT INTO `candidates` VALUES ('247', 'JOSEPH', '2', 'WHITEHALL', 'PA', '2000-08-08', 'This will be the resume');
INSERT INTO `candidates` VALUES ('250', 'CHARLES', '2', 'BADEN', 'PA', '2002-02-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('251', 'LAURIE', '2', 'PITTSBURGH', 'PA', '2001-01-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('253', 'EDWARD', '2', 'PITTSBURGH', 'PA', '1999-02-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('266', 'KENNETH', '2', 'N HUNTINGDON', 'PA', '2000-01-21', 'This will be the resume');
INSERT INTO `candidates` VALUES ('268', 'STEVEN', '2', 'HERMITAGE', 'PA', '2001-10-12', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21650', 'A', '2', 'MCKEES ROCKS', 'PA', '2005-07-22', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21653', 'ROBERT', '2', 'MOON TOWNSHIP', 'PA', '2004-06-22', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21654', 'DAVID', '2', 'ALIQUIPPA', 'PA', '1999-07-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21655', 'JEFFREY', '2', 'BUTLER', 'PA', '2003-11-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21657', 'SEYED', '2', 'GLENSHAW', 'PA', '2003-12-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('21659', 'LYNNE', '2', 'AMBRIDGE', 'PA', '2001-04-06', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25255', 'SANJAY', '2', 'PITTSBURGH', 'PA', '2003-12-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25256', 'DAVID', '2', 'LOWER BURRELL', 'PA', '2006-07-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25257', 'JEANNE', '2', 'WHITE OAK', 'PA', '2004-02-27', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25258', 'TODD', '2', 'PITTSBURGH', 'PA', '2000-03-28', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25259', 'DAVID', '2', 'GRAMPIAN', 'PA', '2003-12-15', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25260', 'MICHAEL', '2', 'CARNEGIE', 'PA', '2003-09-19', 'This will be the resume');
INSERT INTO `candidates` VALUES ('25262', 'BERNARD', '2', 'NORTH VERSAILLES', 'PA', '2003-12-09', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325221', 'Aaron', '3', 'Fountain Valley', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325222', 'Pablo', '3', 'El Cajon', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325223', 'Mitchell', '3', 'Anaheim', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325224', 'Ngoc', '3', 'san diego', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325225', 'David', '3', 'Los Angeles', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325226', 'Ryan', '3', 'Arroyo Grande', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325227', 'Madhusudan', '3', 'Azusa', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325228', 'Patrice', '3', 'Aliso Viejo', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325229', 'Huy', '3', 'Westminster', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325230', 'Alex', '3', 'Oak Park', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325232', 'Harsha', '3', 'Aliso Viejo', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325233', 'krunal', '3', 'Los Angeles', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325234', 'GAREN', '3', 'PASADENA', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325235', 'Reza', '3', 'Sylmar', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325236', 'Sam', '3', 'Mission Hills', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325237', 'Robert', '3', 'Camarillo', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325238', 'Igor', '3', 'La Crescenta', 'CA', '2011-04-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325239', 'shawn', '3', 'TOLUCA LAKE', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325240', 'John', '3', 'Troy', 'OH', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325241', 'Sydney', '3', 'Corona', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325242', 'Yayi', '3', 'Tenafly', 'NJ', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325243', 'John', '3', 'New Knoxville', 'TN', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325244', 'Kurt', '3', 'Highlands Ranch', 'CO', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325245', 'Joe', '3', 'Ashippun', 'WI', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325246', 'Fardin', '3', 'San Ramon', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325247', 'Bud', '3', 'Madison Heights', 'MI', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325248', 'Maria', '3', 'Burlington', 'MA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325249', 'manuel', '3', 'visalia', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325250', 'Jeremy', '3', 'Singer Island', 'FL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325251', 'Terence', '3', 'Orlando', 'FL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325252', 'Martin', '3', 'Allentown', 'PA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325253', 'Carlos', '3', '', 'MA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325254', 'Jason', '3', 'Waxhaw', 'NC', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325255', 'Matthew', '3', '', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325256', 'Gregory', '3', 'Hobart', 'IN', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325257', 'Yoshiro', '3', 'Lowell', 'MA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325258', 'Shaomin', '3', 'Irvine', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325259', 'Claudio', '3', 'Des Plaines', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325260', 'Janak', '3', 'SAN JOSE', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325261', 'James', '3', 'Los Angeles', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325262', 'John', '3', 'Concord', 'TN', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325263', 'James', '3', 'Oxford', 'AL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325264', 'Karl', '3', 'Silver Creek', 'GA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325265', 'David', '3', 'Lutz', 'FL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325266', 'Viralkumar', '3', 'methuen', 'MA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325267', 'Kanchi', '3', 'Chino Hills', 'CA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325268', 'Kamal', '3', 'Schaumburg', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325269', 'Dina', '3', 'Hanover Park', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325270', 'Riad', '3', 'Bridgeton', 'MO', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325271', 'NEHA', '3', 'Crystal Lake', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325272', 'Jatin', '3', 'Tinton Falls', 'NJ', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325273', 'Shreyas', '3', 'Arlington', 'VA', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325274', 'Ishwar', '3', 'st peters', 'IL', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325275', 'Angelica', '3', 'Georgetown', 'TX', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325276', 'Deepen', '3', 'Lawrenceville', 'NJ', '2012-04-13', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325277', 'Sergio', '3', 'San Diego', 'CA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325278', 'Dominick', '3', 'Humble', 'TX', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325280', 'Christopher', '3', 'Grand Blanc', 'MI', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325281', 'ALEXANDER', '3', 'San Diego', 'CA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325282', 'Eric', '3', 'Washington', 'DC', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325283', 'George', '3', 'Orange', 'TX', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325285', 'Eric', '3', 'Valley Cottage', 'NY', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325286', 'Patrick', '3', 'Great Falls', 'VA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325287', 'Lacy', '3', 'North Huntingdon', 'PA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325288', 'Daniel', '3', 'Columbiana', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325289', 'Ryan', '3', 'Dublin', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325290', 'Josef', '3', 'Astoria', 'NY', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325291', 'Hind (Heidi)', '3', 'Cleveland', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325292', 'Sean', '3', 'Jamestown', 'RI', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325293', 'Ronald', '3', 'Manassas', 'VA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325294', 'Jose', '3', 'Miami', 'FL', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325295', 'Cris', '3', 'Cincinnati', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325296', 'Kirk', '3', 'Brooksville', 'FL', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325297', 'Leonard', '3', 'Avila Beach', 'CA', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325298', 'Clayton', '3', 'Columbus', 'OH', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325299', 'Nathan', '3', 'Peru', 'IN', '2012-04-16', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325300', 'Dale', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325301', 'Gary', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325302', 'Robert', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325303', 'Michael', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325304', 'Mark', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325305', 'YU', '3', 'Pittsburgh', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325306', 'Mark', '3', 'Monroeville', 'PA', '2012-04-25', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325307', 'SIDDIQUE', '3', 'Corona', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325308', 'Gayed', '3', 'Alhambra', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325309', 'James', '3', 'Rancho Cucamonga', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325311', 'Steve', '3', 'Torrance', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325312', 'John', '3', 'Los Angeles', 'CA', '2012-04-26', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325313', 'Michael', '3', 'New Kensington', 'PA', '2012-05-02', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325314', 'Eugene', '3', 'Freedom', 'PA', '2012-05-02', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325316', 'Craig', '3', 'Santa Clara', 'CA', '2012-05-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325317', 'Karen', '3', 'Humble', 'TX', '2012-05-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325318', 'Sevko', '3', 'Columbia', 'MD', '2012-05-03', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325319', 'Peter', '3', 'Cleburne', 'TX', '2012-05-08', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325320', 'Mohammed', '3', 'Knoxville', 'TN', '2012-05-04', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325321', 'Patrick', '3', 'Ballwin', 'MO', '2012-05-08', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325322', 'Rafael', '3', 'Orangevale', 'CA', '2011-01-01', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325323', 'Alfredo', '3', 'Kabul', 'CA', '2012-05-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325324', 'Tad', '3', 'Pittsburgh', 'PA', '2012-05-24', 'This will be the resume');
INSERT INTO `candidates` VALUES ('325325', 'Sandchez', '3', 'West Mifflin', 'PA', '2012-05-24', 'This will be the resume');

-- ----------------------------
-- Table structure for `country`
-- ----------------------------
DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `countryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countryName` varchar(50) DEFAULT NULL,
  `isActive` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`countryID`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of country
-- ----------------------------
INSERT INTO `country` VALUES ('1', 'United States', '1');
INSERT INTO `country` VALUES ('2', 'Canada', '0');
INSERT INTO `country` VALUES ('3', 'Mexico', '0');
INSERT INTO `country` VALUES ('4', 'Afghanistan', '0');
INSERT INTO `country` VALUES ('5', 'Albania', '0');
INSERT INTO `country` VALUES ('6', 'Algeria', '0');
INSERT INTO `country` VALUES ('7', 'Andorra', '0');
INSERT INTO `country` VALUES ('8', 'Angola', '0');
INSERT INTO `country` VALUES ('9', 'Anguilla', '0');
INSERT INTO `country` VALUES ('10', 'Antarctica', '0');
INSERT INTO `country` VALUES ('11', 'Antigua and Barbuda', '0');
INSERT INTO `country` VALUES ('12', 'Argentina', '0');
INSERT INTO `country` VALUES ('13', 'Armenia', '0');
INSERT INTO `country` VALUES ('14', 'Aruba', '0');
INSERT INTO `country` VALUES ('15', 'Ascension Island', '0');
INSERT INTO `country` VALUES ('16', 'Australia', '0');
INSERT INTO `country` VALUES ('17', 'Austria', '0');
INSERT INTO `country` VALUES ('18', 'Azerbaijan', '0');
INSERT INTO `country` VALUES ('19', 'Bahamas', '0');
INSERT INTO `country` VALUES ('20', 'Bahrain', '0');
INSERT INTO `country` VALUES ('21', 'Bangladesh', '0');
INSERT INTO `country` VALUES ('22', 'Barbados', '0');
INSERT INTO `country` VALUES ('23', 'Belarus', '0');
INSERT INTO `country` VALUES ('24', 'Belgium', '0');
INSERT INTO `country` VALUES ('25', 'Belize', '0');
INSERT INTO `country` VALUES ('26', 'Benin', '0');
INSERT INTO `country` VALUES ('27', 'Bermuda', '0');
INSERT INTO `country` VALUES ('28', 'Bhutan', '0');
INSERT INTO `country` VALUES ('29', 'Bolivia', '0');
INSERT INTO `country` VALUES ('30', 'Bophuthatswana', '0');
INSERT INTO `country` VALUES ('31', 'Bosnia-Herzegovina', '0');
INSERT INTO `country` VALUES ('32', 'Botswana', '0');
INSERT INTO `country` VALUES ('33', 'Bouvet Island', '0');
INSERT INTO `country` VALUES ('34', 'Brazil', '0');
INSERT INTO `country` VALUES ('35', 'British Indian Ocean', '0');
INSERT INTO `country` VALUES ('36', 'British Virgin Islands', '0');
INSERT INTO `country` VALUES ('37', 'Brunei Darussalam', '0');
INSERT INTO `country` VALUES ('38', 'Bulgaria', '0');
INSERT INTO `country` VALUES ('39', 'Burkina Faso', '0');
INSERT INTO `country` VALUES ('40', 'Burundi', '0');
INSERT INTO `country` VALUES ('41', 'Cambodia', '0');
INSERT INTO `country` VALUES ('42', 'Cameroon', '0');
INSERT INTO `country` VALUES ('44', 'Cape Verde Island', '0');
INSERT INTO `country` VALUES ('45', 'Cayman Islands', '0');
INSERT INTO `country` VALUES ('46', 'Central Africa', '0');
INSERT INTO `country` VALUES ('47', 'Chad', '0');
INSERT INTO `country` VALUES ('48', 'Channel Islands', '0');
INSERT INTO `country` VALUES ('49', 'Chile', '0');
INSERT INTO `country` VALUES ('50', 'China, Peoples Republic', '0');
INSERT INTO `country` VALUES ('51', 'Christmas Island', '0');
INSERT INTO `country` VALUES ('52', 'Cocos (Keeling) Islands', '0');
INSERT INTO `country` VALUES ('53', 'Colombia', '0');
INSERT INTO `country` VALUES ('54', 'Comoros Islands', '0');
INSERT INTO `country` VALUES ('55', 'Congo', '0');
INSERT INTO `country` VALUES ('56', 'Cook Islands', '0');
INSERT INTO `country` VALUES ('57', 'Costa Rica', '0');
INSERT INTO `country` VALUES ('58', 'Croatia', '0');
INSERT INTO `country` VALUES ('59', 'Cuba', '0');
INSERT INTO `country` VALUES ('60', 'Cyprus', '0');
INSERT INTO `country` VALUES ('61', 'Czech Republic', '0');
INSERT INTO `country` VALUES ('62', 'Denmark', '0');
INSERT INTO `country` VALUES ('63', 'Djibouti', '0');
INSERT INTO `country` VALUES ('64', 'Dominica', '0');
INSERT INTO `country` VALUES ('65', 'Dominican Republic', '0');
INSERT INTO `country` VALUES ('66', 'Easter Island', '0');
INSERT INTO `country` VALUES ('67', 'Ecuador', '0');
INSERT INTO `country` VALUES ('68', 'Egypt', '0');
INSERT INTO `country` VALUES ('69', 'El Salvador', '0');
INSERT INTO `country` VALUES ('70', 'England', '0');
INSERT INTO `country` VALUES ('71', 'Equatorial Guinea', '0');
INSERT INTO `country` VALUES ('72', 'Estonia', '0');
INSERT INTO `country` VALUES ('73', 'Ethiopia', '0');
INSERT INTO `country` VALUES ('74', 'Falkland Islands', '0');
INSERT INTO `country` VALUES ('75', 'Faeroe Islands', '0');
INSERT INTO `country` VALUES ('76', 'Fiji', '0');
INSERT INTO `country` VALUES ('77', 'Finland', '0');
INSERT INTO `country` VALUES ('78', 'France', '0');
INSERT INTO `country` VALUES ('79', 'French Guyana', '0');
INSERT INTO `country` VALUES ('80', 'French Polynesia', '0');
INSERT INTO `country` VALUES ('81', 'Gabon', '0');
INSERT INTO `country` VALUES ('82', 'Gambia', '0');
INSERT INTO `country` VALUES ('83', 'Georgia Republic', '0');
INSERT INTO `country` VALUES ('84', 'Germany', '0');
INSERT INTO `country` VALUES ('85', 'Gibraltar', '0');
INSERT INTO `country` VALUES ('86', 'Greece', '0');
INSERT INTO `country` VALUES ('87', 'Greenland', '0');
INSERT INTO `country` VALUES ('88', 'Grenada', '0');
INSERT INTO `country` VALUES ('89', 'Guadeloupe (French)', '0');
INSERT INTO `country` VALUES ('90', 'Guatemala', '0');
INSERT INTO `country` VALUES ('91', 'Guernsey Island', '0');
INSERT INTO `country` VALUES ('92', 'Guinea Bissau', '0');
INSERT INTO `country` VALUES ('93', 'Guinea', '0');
INSERT INTO `country` VALUES ('94', 'Guyana', '0');
INSERT INTO `country` VALUES ('95', 'Haiti', '0');
INSERT INTO `country` VALUES ('96', 'Heard and McDonald Isls', '0');
INSERT INTO `country` VALUES ('97', 'Honduras', '0');
INSERT INTO `country` VALUES ('98', 'Hong Kong', '0');
INSERT INTO `country` VALUES ('99', 'Hungary', '0');
INSERT INTO `country` VALUES ('100', 'Iceland', '0');
INSERT INTO `country` VALUES ('101', 'India', '0');
INSERT INTO `country` VALUES ('102', 'Iran', '0');
INSERT INTO `country` VALUES ('103', 'Iraq', '0');
INSERT INTO `country` VALUES ('104', 'Ireland', '0');
INSERT INTO `country` VALUES ('105', 'Isle of Man', '0');
INSERT INTO `country` VALUES ('106', 'Israel', '0');
INSERT INTO `country` VALUES ('107', 'Italy', '0');
INSERT INTO `country` VALUES ('108', 'Ivory Coast', '0');
INSERT INTO `country` VALUES ('109', 'Jamaica', '0');
INSERT INTO `country` VALUES ('110', 'Japan', '0');
INSERT INTO `country` VALUES ('111', 'Jersey Island', '0');
INSERT INTO `country` VALUES ('112', 'Jordan', '0');
INSERT INTO `country` VALUES ('113', 'Kazakhstan', '0');
INSERT INTO `country` VALUES ('114', 'Kenya', '0');
INSERT INTO `country` VALUES ('115', 'Kiribati', '0');
INSERT INTO `country` VALUES ('116', 'Kuwait', '0');
INSERT INTO `country` VALUES ('117', 'Laos', '0');
INSERT INTO `country` VALUES ('118', 'Latvia', '0');
INSERT INTO `country` VALUES ('119', 'Lebanon', '0');
INSERT INTO `country` VALUES ('120', 'Lesotho', '0');
INSERT INTO `country` VALUES ('121', 'Liberia', '0');
INSERT INTO `country` VALUES ('122', 'Libya', '0');
INSERT INTO `country` VALUES ('123', 'Liechtenstein', '0');
INSERT INTO `country` VALUES ('124', 'Lithuania', '0');
INSERT INTO `country` VALUES ('125', 'Luxembourg', '0');
INSERT INTO `country` VALUES ('126', 'Macao', '0');
INSERT INTO `country` VALUES ('127', 'Macedonia', '0');
INSERT INTO `country` VALUES ('128', 'Madagascar', '0');
INSERT INTO `country` VALUES ('129', 'Malawi', '0');
INSERT INTO `country` VALUES ('130', 'Malaysia', '0');
INSERT INTO `country` VALUES ('131', 'Maldives', '0');
INSERT INTO `country` VALUES ('132', 'Mali', '0');
INSERT INTO `country` VALUES ('133', 'Malta', '0');
INSERT INTO `country` VALUES ('134', 'Martinique (French)', '0');
INSERT INTO `country` VALUES ('135', 'Mauritania', '0');
INSERT INTO `country` VALUES ('136', 'Mauritius', '0');
INSERT INTO `country` VALUES ('137', 'Mayotte', '0');
INSERT INTO `country` VALUES ('139', 'Micronesia', '0');
INSERT INTO `country` VALUES ('140', 'Moldavia', '0');
INSERT INTO `country` VALUES ('141', 'Monaco', '0');
INSERT INTO `country` VALUES ('142', 'Mongolia', '0');
INSERT INTO `country` VALUES ('143', 'Montenegro', '0');
INSERT INTO `country` VALUES ('144', 'Montserrat', '0');
INSERT INTO `country` VALUES ('145', 'Morocco', '0');
INSERT INTO `country` VALUES ('146', 'Mozambique', '0');
INSERT INTO `country` VALUES ('147', 'Myanmar', '0');
INSERT INTO `country` VALUES ('148', 'Namibia', '0');
INSERT INTO `country` VALUES ('149', 'Nauru', '0');
INSERT INTO `country` VALUES ('150', 'Nepal', '0');
INSERT INTO `country` VALUES ('151', 'Netherlands Antilles', '0');
INSERT INTO `country` VALUES ('152', 'Netherlands', '0');
INSERT INTO `country` VALUES ('153', 'New Caledonia (French)', '0');
INSERT INTO `country` VALUES ('154', 'New Zealand', '0');
INSERT INTO `country` VALUES ('155', 'Nicaragua', '0');
INSERT INTO `country` VALUES ('156', 'Niger', '0');
INSERT INTO `country` VALUES ('157', 'Niue', '0');
INSERT INTO `country` VALUES ('158', 'Norfolk Island', '0');
INSERT INTO `country` VALUES ('159', 'North Korea', '0');
INSERT INTO `country` VALUES ('160', 'Northern Ireland', '0');
INSERT INTO `country` VALUES ('161', 'Norway', '0');
INSERT INTO `country` VALUES ('162', 'Oman', '0');
INSERT INTO `country` VALUES ('163', 'Pakistan', '0');
INSERT INTO `country` VALUES ('164', 'Panama', '0');
INSERT INTO `country` VALUES ('165', 'Papua New Guinea', '0');
INSERT INTO `country` VALUES ('166', 'Paraguay', '0');
INSERT INTO `country` VALUES ('167', 'Peru', '0');
INSERT INTO `country` VALUES ('168', 'Philippines', '0');
INSERT INTO `country` VALUES ('169', 'Pitcairn Island', '0');
INSERT INTO `country` VALUES ('170', 'Poland', '0');
INSERT INTO `country` VALUES ('171', 'Polynesia (French)', '0');
INSERT INTO `country` VALUES ('172', 'Portugal', '0');
INSERT INTO `country` VALUES ('173', 'Qatar', '0');
INSERT INTO `country` VALUES ('174', 'Reunion Island', '0');
INSERT INTO `country` VALUES ('175', 'Romania', '0');
INSERT INTO `country` VALUES ('176', 'Russia', '0');
INSERT INTO `country` VALUES ('177', 'Rwanda', '0');
INSERT INTO `country` VALUES ('178', 'S.Georgia Sandwich Isls', '0');
INSERT INTO `country` VALUES ('179', 'Sao Tome, Principe', '0');
INSERT INTO `country` VALUES ('180', 'San Marino', '0');
INSERT INTO `country` VALUES ('181', 'Saudi Arabia', '0');
INSERT INTO `country` VALUES ('182', 'Scotland', '0');
INSERT INTO `country` VALUES ('183', 'Senegal', '0');
INSERT INTO `country` VALUES ('184', 'Serbia', '0');
INSERT INTO `country` VALUES ('185', 'Seychelles', '0');
INSERT INTO `country` VALUES ('186', 'Shetland', '0');
INSERT INTO `country` VALUES ('187', 'Sierra Leone', '0');
INSERT INTO `country` VALUES ('188', 'Singapore', '0');
INSERT INTO `country` VALUES ('189', 'Slovak Republic', '0');
INSERT INTO `country` VALUES ('190', 'Slovenia', '0');
INSERT INTO `country` VALUES ('191', 'Solomon Islands', '0');
INSERT INTO `country` VALUES ('192', 'Somalia', '0');
INSERT INTO `country` VALUES ('193', 'South Africa', '0');
INSERT INTO `country` VALUES ('194', 'South Korea', '0');
INSERT INTO `country` VALUES ('195', 'Spain', '0');
INSERT INTO `country` VALUES ('196', 'Sri Lanka', '0');
INSERT INTO `country` VALUES ('197', 'St. Helena', '0');
INSERT INTO `country` VALUES ('198', 'St. Lucia', '0');
INSERT INTO `country` VALUES ('199', 'St. Pierre Miquelon', '0');
INSERT INTO `country` VALUES ('200', 'St. Martins', '0');
INSERT INTO `country` VALUES ('201', 'St. Kitts Nevis Anguilla', '0');
INSERT INTO `country` VALUES ('202', 'St. Vincent Grenadines', '0');
INSERT INTO `country` VALUES ('203', 'Sudan', '0');
INSERT INTO `country` VALUES ('204', 'Suriname', '0');
INSERT INTO `country` VALUES ('205', 'Svalbard Jan Mayen', '0');
INSERT INTO `country` VALUES ('206', 'Swaziland', '0');
INSERT INTO `country` VALUES ('207', 'Sweden', '0');
INSERT INTO `country` VALUES ('208', 'Switzerland', '0');
INSERT INTO `country` VALUES ('209', 'Syria', '0');
INSERT INTO `country` VALUES ('210', 'Tajikistan', '0');
INSERT INTO `country` VALUES ('211', 'Taiwan', '0');
INSERT INTO `country` VALUES ('212', 'Tahiti', '0');
INSERT INTO `country` VALUES ('213', 'Tanzania', '0');
INSERT INTO `country` VALUES ('214', 'Thailand', '0');
INSERT INTO `country` VALUES ('215', 'Togo', '0');
INSERT INTO `country` VALUES ('216', 'Tokelau', '0');
INSERT INTO `country` VALUES ('217', 'Tonga', '0');
INSERT INTO `country` VALUES ('218', 'Trinidad and Tobago', '0');
INSERT INTO `country` VALUES ('219', 'Tunisia', '0');
INSERT INTO `country` VALUES ('220', 'Turkmenistan', '0');
INSERT INTO `country` VALUES ('221', 'Turks and Caicos Isls', '0');
INSERT INTO `country` VALUES ('222', 'Tuvalu', '0');
INSERT INTO `country` VALUES ('223', 'Uganda', '0');
INSERT INTO `country` VALUES ('224', 'Ukraine', '0');
INSERT INTO `country` VALUES ('225', 'United Arab Emirates', '0');
INSERT INTO `country` VALUES ('226', 'Uruguay', '0');
INSERT INTO `country` VALUES ('227', 'Uzbekistan', '0');
INSERT INTO `country` VALUES ('228', 'Vanuatu', '0');
INSERT INTO `country` VALUES ('229', 'Vatican City State', '0');
INSERT INTO `country` VALUES ('230', 'Venezuela', '0');
INSERT INTO `country` VALUES ('231', 'Vietnam', '0');
INSERT INTO `country` VALUES ('232', 'Virgin Islands (Brit)', '0');
INSERT INTO `country` VALUES ('233', 'Wales', '0');
INSERT INTO `country` VALUES ('234', 'Wallis Futuna Islands', '0');
INSERT INTO `country` VALUES ('235', 'Western Sahara', '0');
INSERT INTO `country` VALUES ('236', 'Western Samoa', '0');
INSERT INTO `country` VALUES ('237', 'Yemen', '0');
INSERT INTO `country` VALUES ('238', 'Yugoslavia', '0');
INSERT INTO `country` VALUES ('239', 'Zaire', '0');
INSERT INTO `country` VALUES ('240', 'Zambia', '0');
INSERT INTO `country` VALUES ('241', 'Zimbabwe', '0');

-- ----------------------------
-- Table structure for `discipline`
-- ----------------------------
DROP TABLE IF EXISTS `discipline`;
CREATE TABLE `discipline` (
  `disciplineID` int(11) NOT NULL AUTO_INCREMENT,
  `discipline` varchar(100) NOT NULL,
  PRIMARY KEY (`disciplineID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of discipline
-- ----------------------------
INSERT INTO `discipline` VALUES ('1', 'First Discipline');
INSERT INTO `discipline` VALUES ('2', 'Second Discipline');
INSERT INTO `discipline` VALUES ('3', 'Third Discipline');
INSERT INTO `discipline` VALUES ('4', 'Fourth Discipline');
INSERT INTO `discipline` VALUES ('5', 'Fifth Discipline');
INSERT INTO `discipline` VALUES ('6', 'Sixth Discipline');
INSERT INTO `discipline` VALUES ('7', 'Seventh Discipline');
INSERT INTO `discipline` VALUES ('8', 'Eighth Discipline');
INSERT INTO `discipline` VALUES ('9', 'Ninth Discipline');
INSERT INTO `discipline` VALUES ('10', 'Tenth Discipline');
INSERT INTO `discipline` VALUES ('11', 'Eleventh Discipline');
INSERT INTO `discipline` VALUES ('12', 'Twelveth Discipline');
INSERT INTO `discipline` VALUES ('13', 'Thirteenth Discipline');
INSERT INTO `discipline` VALUES ('14', 'Fourteenth Discipline');
INSERT INTO `discipline` VALUES ('15', 'Fifteenth Discipline');
INSERT INTO `discipline` VALUES ('16', 'Sixteenth Discipline');
INSERT INTO `discipline` VALUES ('17', 'Seventeenth Discipline');
INSERT INTO `discipline` VALUES ('18', 'Eighteenth Discipline');

-- ----------------------------
-- Table structure for `provincestate`
-- ----------------------------
DROP TABLE IF EXISTS `provincestate`;
CREATE TABLE `provincestate` (
  `provinceStateID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `countryID` int(10) unsigned NOT NULL,
  `state` varchar(50) DEFAULT NULL,
  `abbreviation` char(2) DEFAULT NULL,
  PRIMARY KEY (`provinceStateID`),
  KEY `fk_provinceState_country1` (`countryID`) USING BTREE,
  CONSTRAINT `provincestate_ibfk_1` FOREIGN KEY (`countryID`) REFERENCES `country` (`countryID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of provincestate
-- ----------------------------
INSERT INTO `provincestate` VALUES ('1', '2', 'Alaska', 'AK');
INSERT INTO `provincestate` VALUES ('2', '2', 'Alabama', 'AL');
INSERT INTO `provincestate` VALUES ('3', '2', 'Arizona', 'AZ');
INSERT INTO `provincestate` VALUES ('4', '2', 'Arkansas', 'AR');
INSERT INTO `provincestate` VALUES ('5', '2', 'California', 'CA');
INSERT INTO `provincestate` VALUES ('6', '2', 'Colorado', 'CO');
INSERT INTO `provincestate` VALUES ('7', '2', 'Connecticut', 'CT');
INSERT INTO `provincestate` VALUES ('8', '2', 'Delaware', 'DE');
INSERT INTO `provincestate` VALUES ('9', '2', 'District of Columbia', 'DC');
INSERT INTO `provincestate` VALUES ('10', '2', 'Florida', 'FL');
INSERT INTO `provincestate` VALUES ('11', '2', 'Georgia', 'GA');
INSERT INTO `provincestate` VALUES ('12', '2', 'Hawaii', 'HI');
INSERT INTO `provincestate` VALUES ('13', '2', 'Idaho', 'ID');
INSERT INTO `provincestate` VALUES ('14', '2', 'Illinois', 'IL');
INSERT INTO `provincestate` VALUES ('15', '2', 'Indiana', 'IN');
INSERT INTO `provincestate` VALUES ('16', '2', 'Iowa', 'IA');
INSERT INTO `provincestate` VALUES ('17', '2', 'Kansas', 'KS');
INSERT INTO `provincestate` VALUES ('18', '2', 'Kentucky', 'KY');
INSERT INTO `provincestate` VALUES ('19', '2', 'Louisiana', 'LA');
INSERT INTO `provincestate` VALUES ('20', '2', 'Maine', 'ME');
INSERT INTO `provincestate` VALUES ('21', '2', 'Maryland', 'MD');
INSERT INTO `provincestate` VALUES ('22', '2', 'Massachusetts', 'MA');
INSERT INTO `provincestate` VALUES ('23', '2', 'Michigan', 'MI');
INSERT INTO `provincestate` VALUES ('24', '2', 'Minnesota', 'MN');
INSERT INTO `provincestate` VALUES ('25', '2', 'Mississippi', 'MS');
INSERT INTO `provincestate` VALUES ('26', '2', 'Missouri', 'MO');
INSERT INTO `provincestate` VALUES ('27', '2', 'Montana', 'MT');
INSERT INTO `provincestate` VALUES ('28', '2', 'Nebraska', 'NE');
INSERT INTO `provincestate` VALUES ('29', '2', 'Nevada', 'NV');
INSERT INTO `provincestate` VALUES ('30', '2', 'New Hampshire', 'NH');
INSERT INTO `provincestate` VALUES ('31', '2', 'New Jersey', 'NJ');
INSERT INTO `provincestate` VALUES ('32', '2', 'New Mexico', 'NM');
INSERT INTO `provincestate` VALUES ('33', '2', 'New York', 'NY');
INSERT INTO `provincestate` VALUES ('34', '2', 'North Carolina', 'NC');
INSERT INTO `provincestate` VALUES ('35', '2', 'North Dakota', 'ND');
INSERT INTO `provincestate` VALUES ('36', '2', 'Ohio', 'OH');
INSERT INTO `provincestate` VALUES ('37', '2', 'Oklahoma', 'OK');
INSERT INTO `provincestate` VALUES ('38', '2', 'Oregon', 'OR');
INSERT INTO `provincestate` VALUES ('39', '2', 'Pennsylvania', 'PA');
INSERT INTO `provincestate` VALUES ('40', '2', 'Puerto Rico', 'PR');
INSERT INTO `provincestate` VALUES ('41', '2', 'Rhode Island', 'RI');
INSERT INTO `provincestate` VALUES ('42', '2', 'South Carolina', 'SC');
INSERT INTO `provincestate` VALUES ('43', '2', 'South Dakota', 'SD');
INSERT INTO `provincestate` VALUES ('44', '2', 'Tennessee', 'TN');
INSERT INTO `provincestate` VALUES ('45', '2', 'Texas', 'TX');
INSERT INTO `provincestate` VALUES ('46', '2', 'Utah', 'UT');
INSERT INTO `provincestate` VALUES ('47', '2', 'Vermont', 'VT');
INSERT INTO `provincestate` VALUES ('48', '2', 'Virginia', 'VA');
INSERT INTO `provincestate` VALUES ('49', '2', 'Washington', 'WA');
INSERT INTO `provincestate` VALUES ('50', '2', 'West Virginia', 'WV');
INSERT INTO `provincestate` VALUES ('51', '2', 'Wisconsin', 'WI');
INSERT INTO `provincestate` VALUES ('52', '2', 'Wyoming', 'WY');

-- ----------------------------
-- Table structure for `session`
-- ----------------------------
DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `session_id` int(11) DEFAULT NULL,
  `id` char(32) NOT NULL DEFAULT '',
  `data` text,
  `name` varchar(50) DEFAULT NULL,
  `save_path` varchar(100) DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `lifetime` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of session
-- ----------------------------

-- ----------------------------
-- Table structure for `usertype`
-- ----------------------------
DROP TABLE IF EXISTS `usertype`;
CREATE TABLE `usertype` (
  `userTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userType` varchar(50) DEFAULT NULL,
  `isActive` bit(1) DEFAULT NULL,
  PRIMARY KEY (`userTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usertype
-- ----------------------------
