<?php
class Application_Form_Register extends Zend_Form {
	public function init() {
		$this->setMethod('post');
		
		$this->addElement(
            'text', 'firstname', array(
                'label' => 'First Name:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));
            
        $this->addElement(
            'text', 'lastname', array(
                'label' => 'Last Name:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));
            
        $this->addElement(
            'text', 'email', array(
                'label' => 'Email:',
                'required' => true,
                'filters'    => array('StringTrim'),
            ));
                   
        $this->addElement('password', 'password', array(
            'label' => 'Password:',
            'required' => true,
            ));

         $this->addElement('password', 'password2', array(
            'label' => 'Confirm Password:',
            'required' => true,
            ));
 
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Register',
            ));
    }
		
}