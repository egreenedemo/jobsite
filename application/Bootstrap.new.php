<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initDoctype() {
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->doctype('HTML5');
	}

	protected function _initAutoload() {
		$modelLoader = new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Application',
                    'basePath' => APPLICATION_PATH
		));

		return $modelLoader;
	}

	protected function _initSession() {
		$dbOption = $this->getOption('resources');
		$dbOption = $dbOption['db'];

		$db = Zend_Db::factory('Pdo_Mysql', array(
            'host'        =>$dbOption['params']['host'],
            'username'    => $dbOption['params']['username'],
            'password'    =>$dbOption['params']['password'],
            'dbname'    => $dbOption['params']['dbname'],
		    'dsn' =>$dbOption['params']['dsn']
		));
		Zend_Db_Table_Abstract::setDefaultAdapter($db);

		/*$sessionConfig = array(
            'name'           => 'session',      
            'primary'        => 'id',   
            'modifiedColumn' => 'modified',     
            'dataColumn'     => 'data',
            'lifetimeColumn' => 'lifetime'
            );
            $saveHandler = new Zend_Session_SaveHandler_DbTable($sessionConfig);
            Zend_Session::setSaveHandler($saveHandler);
            */
            Zend_Session::start();

            $pjsession = new Zend_Session_Namespace('peakJobs');
            $pjsession->loggedOn = 0;
	
            return $pjsession;
	}

	protected function _initDb() {

		$dbOption = $this->getOption('resources');
		$dbOption = $dbOption['db'];

		// Setup database
		$db = Zend_Db::factory('Pdo_Mysql', array(
            'host'        =>$dbOption['params']['host'],
            'username'    => $dbOption['params']['username'],
            'password'    =>$dbOption['params']['password'],
            'dbname'    => $dbOption['params']['dbname'],
		    'dsn' 		=> $dbOption['params']['dsn']));

		$db->setFetchMode(Zend_Db::FETCH_ASSOC);
		// $db->query("SET NAMES 'utf8'");
		// $db->query("SET CHARACTER SET 'utf8'");

		Zend_Registry::set('connectDB', $db);

		Zend_Db_Table::setDefaultAdapter($db);

		// Return it, so that it can be stored by the bootstrap
		return $db;
	}

	protected function _initRequest() {
		/*Zend_Session::start();
		$pjsession = new Zend_Session_Namespace('peakJobs');
		
		if(!isset($pjsession->loggedOn)) {
			$pjsession->loggedOn = 0;
		}
		*/
	}
	
	protected function _initCache() {
		$cacheOptions = $this->getOption('cache');
		$appCache = null;
		
		/*if($cacheOptions['useCache'] == 1) {
			$frontendOptions = array(
				'lifetime' => 3600, // 1 hour
				'automatic_serialization' => true
			);
	
			$cacheServers = $cacheOptions['cacheServers'];
		

			$backendOptions = array(
				'servers'=>$cacheServers, 'compression'=>false, 'compatibility'=>true // an array of options from config.ini
			);
			
			$appCache = Zend_Cache::factory('Core','Memcached',$frontendOptions, $backendOptions);
		}
		*/
		Zend_Registry::set('cacheUsed',$cacheOptions['useCache']);
		Zend_Registry::set('appCache',$appCache);
		
		return $appCache;
	}
}

