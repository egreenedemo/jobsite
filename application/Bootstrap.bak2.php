<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initDoctype() {
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->doctype('HTML5');
	}

	protected function _initAutoload() {
		$modelLoader = new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Application',
                    'basePath' => APPLICATION_PATH
		));

		return $modelLoader;
	}

	protected function _initSession() {
		$dbOption = $this->getOption('resources');
		$dbOption = $dbOption['db'];

		$db = Zend_Db::factory('Pdo_Mysql', array(
            'host'        =>$dbOption['params']['host'],
            'username'    => $dbOption['params']['username'],
            'password'    =>$dbOption['params']['password'],
            'dbname'    => $dbOption['params']['dbname'],
		    'dsn' =>$dbOption['params']['dsn']
		));
		Zend_Db_Table_Abstract::setDefaultAdapter($db);

		$sessionConfig = array(
            'name'           => 'session',      
            'primary'        => 'id',   
            'modifiedColumn' => 'modified',     
            'dataColumn'     => 'data',
            'lifetimeColumn' => 'lifetime'
            );
            $saveHandler = new Zend_Session_SaveHandler_DbTable($sessionConfig);
            Zend_Session::setSaveHandler($saveHandler);
           
            Zend_Session::start();

            $pjsession = new Zend_Session_Namespace('peakJobs');
            $pjsession->loggedOn = 0;
	
            return $pjsession;
	}

	protected function _initDb() {

		$dbOption = $this->getOption('resources');
		$dbOption = $dbOption['db'];

		// Setup database
		$db = Zend_Db::factory('Pdo_Mysql', array(
            'host'        =>$dbOption['params']['host'],
            'username'    => $dbOption['params']['username'],
            'password'    =>$dbOption['params']['password'],
            'dbname'    => $dbOption['params']['dbname'],
		    'dsn' 		=> $dbOption['params']['dsn']));

		$db->setFetchMode(Zend_Db::FETCH_ASSOC);
		// $db->query("SET NAMES 'utf8'");
		// $db->query("SET CHARACTER SET 'utf8'");

		Zend_Registry::set('connectDB', $db);

		Zend_Db_Table::setDefaultAdapter($db);

		// Return it, so that it can be stored by the bootstrap
		return $db;
	}

	protected function _initRequest() {
		/*	Zend_Session::start();
		$pjsession = new Zend_Session_Namespace('peakJobs');
	
		if(!isset($pjsession->loggedOn)) {
			$pjsession->loggedOn = 0;
		}
		
		return $pjsession;
		*/
	}
	
	protected function _initCache() {
		$cacheOptions = $this->getOption('cache');
		$appCache = null;
		
		if($cacheOptions['useCache'] == 1) {
			$frontendOptions = array(
				'lifetime' => 3600, // 1 hour
				'automatic_serialization' => true
			);
	
			$cacheServers = $cacheOptions['cacheServers'];
		

			$backendOptions = array(
				'servers'=>$cacheServers, 'compression'=>false, 'compatibility'=>true // an array of options from config.ini
			);
			
			$appCache = Zend_Cache::factory('Core','Memcached',$frontendOptions, $backendOptions);
		}
		
		Zend_Registry::set('cacheUsed',$cacheOptions['useCache']);
		Zend_Registry::set('appCache',$appCache);
			
		return $appCache;
	}
	
	protected function _initRouter() {
		$ctrl  = Zend_Controller_Front::getInstance();
		$router = $ctrl->getRouter(); // returns a rewrite router by default

			$route = new Zend_Controller_Router_Route_Regex(
		    'candidates/(?!candidates)(Alabama|Alaska|Arizona|Arkansas|California|Colorado|Connecticut|Delaware|Florida|Georgia|Hawaii|Idaho|Illinois|Indiana|Iowa|Kansas|Kentucky|Louisiana|Maine|Maryland|Massachusetts|Michigan|Minnesota|Mississippi|Missouri|Montana|Nebraska|Nevada|New\sHampshire|New\sJersey|New\sMexico|New\sYork|North\sCarolina|North\sDakota|Ohio|Oklahoma|Oregon|Pennsylvania|Rhode\sIsland|South\sCarolina|South\sDakota|Tennessee|Texas|Utah|Vermont|Virginia|Washington|West\sVirginia|Wisconsin|Wyoming)',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
		    array(
		        1 => 'state'
		    ),
   			 'candidates/%s'
		);
		
		$router->addRoute('states', $route);
		
		$route = new Zend_Controller_Router_Route_Regex(
		    'candidates/(?!candidates)([a-zA-Z]+\-{1}[a-zA-Z]+)$',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
		    array(
		        1 => 'discipline',
		    ),
   			 'candidates/%s'
		);

		$router->addRoute('disciplines', $route);
		
			
		$route = new Zend_Controller_Router_Route_Regex(
		    'candidates/([A-Z]{2})\/{1}([a-zA-Z\s\-]+)',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
		    array(
		        1 => 'state',
		        2 => 'city'
		    ),
   			 'candidates/%s'
		);
		
		$router->addRoute('cities', $route);
		
		return $router;
		
	}
	
	protected function _initConfig()
	{
		$config = new Zend_Config($this->getOptions());
		Zend_Registry::set('config', $config);
		return $config;
	}	
}

