function showAlert(tmpCandidateID) {
	$.ajax({
		  type: "GET",
		  url: "/candidates/save",
		  data: { candidateID: tmpCandidateID }
		}).done(function( msg ) {
			$(".candidateSavedAlert").show().alert();
		});
	//$(".candidateSavedAlert").alert('close');
}

$(function ()  
{
	$('#saveResume').popover();
});

function switchFilterBy(thisFilter) {
	var filters = ['discipline','state','city'];
	var displayType = '';
	var classType = '';
	
	for(var i=0;i<filters.length;i++) {
		if(filters[i] == thisFilter) {
			displayType = 'inline';
			classType = 'active';
		} else {
			displayType = 'none';
			classType = 'inactive';
		}
		//alert(document.getElementById(filters[i]).style.display);
		document.getElementById(filters[i]).style.display = displayType;
		document.getElementById(filters[i]+'Link').className = classType;
	}
}