<?php

class Application_Model_DisciplineMapper
{
	protected $_dbTable;

	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}

	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Discipline');
		}
		return $this->_dbTable;
	}

	public function save(Application_Model_Disclipline $disclipline)
	{
		$data = array(
        	'discipline' => $discipline->getDiscipline(),
			'disciplineID' => $disclipline->getDisciplineID()
		);

		if (null === ($disciplineID = $discipline->getDisciplineID())) {
			unset($data['disciplineID']);
			$this->getDbTable()->insert($data);
		} else {
			$this->getDbTable()->update($data, array('disciplineID = ?' => $disciplineID));
		}
		
		$appCache->save($data, 'discipline_'.$disciplineID,array('discipline')); // adding discipline word to ID and tag because UUID is not currently used as primary key
	}

	/* find - search for discipline by disciplineID */
	public function find($disciplineID, Application_Model_Discipline $discipline)
	{
		$cacheUsed = Zend_Registry::get('cacheUsed');
		$appCache = Zend_Registry::get('appCache');
		$cacheLifetime = Zend_Registry::get('cacheLifetime');

		try {
			$discipline = $appCache->load("getDiscipline_".$disciplineID);
			if(is_array($discipline) && count($discipline) > 0) {
				$cacheLoaded = true;
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}
		
		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) {
			$result = $this->getDbTable()->find($disciplineID);	
		}
		
		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$discipline->setActive($row->discipline);
		
		$appCache->save($disciplines,"getDiscipline_".$disciplineID,array(),3600);
		
		return $discipline;
	}

	/* fetchAll - Get all of the disciplines in the database */
	public function fetchAll()
	{
		//$cacheUsed = Zend_Registry::get('cacheUsed');
		//$appCache = Zend_Registry::get('appCache');
		$rowIds = null;
		$disciplines   = array();
					
	/*	if($cacheUsed == 1) {
			try {
				$rowIds = $appCache->getIdsMatchingTags('discipline');
			} catch (Exception $e) {
				print_r($appCache->getBackend());
			}

			if($rowIds != null) {
				foreach ($rowIds as $rowId) {
					$row = $appCache->load($rowId);
					
					$discipline = new Application_Model_Discipline();
					$discipline->setDiscipline($row->discipline)
						->setDisciplineID($row->disciplineID);
					$disciplines[] = $discipline;
				}	
			}
		} else {
			*/	
			$resultSet = $this->getDbTable()->fetchAll();
	
			foreach ($resultSet as $row) {
				$discipline = new Application_Model_Discipline();
				$discipline->setDiscipline($row->discipline)
					->setDisciplineID($row->disciplineID);
				$disciplines[] = $discipline;
		//	}
		}
		return $disciplines;
	}
	
	/* getIndexDisciplines */
	public function getIndexDisciplines() {
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
		$appCache = Zend_Registry::get('appCache');
		$cacheLifetime = Zend_Registry::get('cacheLifetime');
				
		$resultSet = null;
		$cacheLoaded = false;
		
		$disciplines = array();
		
		try {
			if(isset($appCache)) {
				$disciplines = $appCache->load("indexDisciplines");
				if(is_array($disciplines) && count($disciplines) > 0) {
					$cacheLoaded = true;
				}
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}
		
		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) { //!is_array($disciplines) || count($disciplines) == 0

			$sql = "SELECT DISTINCT d.discipline, d.disciplineID
						FROM `candidates` c
							INNER JOIN `discipline` d ON d.disciplineID = c.disciplineID
						ORDER BY d.discipline";
			
			$resultSet = $db->query($sql);
		}
		
		if($resultSet != null) {
			foreach($resultSet as $row) {
				$discipline = new Application_Model_Discipline();
				$discipline->setDiscipline($row["discipline"])
						->setDisciplineID($row["disciplineID"]);
				$disciplines[] = $discipline;
			}
		
			if(isset($appCache)) {
				$appCache->save($disciplines,"indexDisciplines",array(),$cacheLifetime);	
			}
		}
		return $disciplines;
	}
	
/* getCandidateDisciplines */
	public function getCandidateDisciplines($state="",$city="") {
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
		$appCache = Zend_Registry::get('appCache');
		$cacheLifetime = Zend_Registry::get('cacheLifetime');
				
		$resultSet = null;
		$cacheLoaded = false;
		
		$sqlparams = array();
		$disciplines = array();
		
		try {
			if(isset($appCache)) {
				$disciplines = $appCache->load("candidateDisciplines"."_".str_replace(" ","",$state)."_".str_replace(" ","",$city));
				if(is_array($disciplines) && count($disciplines) > 0) {
					$cacheLoaded = true;
				}
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}
		
		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) { //!is_array($disciplines) || count($disciplines) == 0

			if(strlen($state) > 0) {
				array_push($sqlparams, $state);
				array_push($sqlparams, $state);
			}
			
			if(strlen($city) >  0) {
				array_push($sqlparams, $city);
			}
			
			$sql = "SELECT DISTINCT d.discipline, d.disciplineID
						FROM zipcodes z
							INNER JOIN candidates c ON c.postalCode = z.postalCode
							INNER JOIN `discipline` d ON d.disciplineID = c.disciplineID
							INNER JOIN msaCodes m ON m.msaCode = z.MSACode
						WHERE 1=1 ";
		   
			if(strlen($state) > 0 ) {
				$sql.= " AND ( z.ProvinceName = ? OR z.ProvinceAbb = ? ) ";
			}
			
			if(strlen($city) > 0 ) {
				$sql.= " AND  SUBSTRING_INDEX(SUBSTRING_INDEX(m.MsaName,',',1),'-',1) = ? ";
			}

			$sql .= " ORDER BY d.discipline";
			
			if(count($sqlparams) > 0) {
				$resultSet = $db->query($sql,$sqlparams);
			} else {
				$resultSet = $db->query($sql);
			}
		}
		
		if($resultSet != null) {
			foreach($resultSet as $row) {
				$discipline = new Application_Model_Discipline();
				$discipline->setDiscipline($row["discipline"])
						->setDisciplineID($row["disciplineID"]);
				$disciplines[] = $discipline;
			}
		
			if(isset($appCache)) {
				$appCache->save($disciplines,"candidateDisciplines"."_".str_replace(" ","",$state)."_".str_replace(" ","",$city),array(),$cacheLifetime);	
			}
		}
		return $disciplines;
	}
}

