<?php

class Application_Model_Candidates
{
	protected $candidates;
	protected $candidateID;
	protected $firstName;
	protected $disciplineID;
	protected $discipline;
	protected $city;
	protected $state;
	protected $postalCode;
	protected $updated;
	protected $resume;
	protected $metrocity;
	protected $metrostate;

	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value)
	{
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Candidates property');
		}
		$this->$method($value);
	}

	public function __get($name)
	{
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Candidates property');
		}
		return $this->$method();
	}

	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}

	public function setCandidateID($text)
	{
		$this->candidateID = (int) $text;
		return $this;
	}

	public function getCandidateID()
	{
		return $this->candidateID;
	}

	public function setFirstName($text)
	{
		$this->firstName = (string) $text;
		return $this;
	}

	public function getFirstName()
	{
		return $this->firstName;
	}

	public function setDisciplineID($text)
	{
		$this->disciplineID = $text;
		return $this;
	}

	public function getDisciplineID()
	{
		return $this->disciplineID;
	}

	public function setDiscipline($text)
	{
		$this->discipline = $text;
		return $this;
	}

	public function getDiscipline()
	{
		return $this->discipline;
	}

	public function setCity($text)
	{
		$this->city = (string) $text;
		return $this;
	}

	public function getCity()
	{
		return $this->city;
	}

	public function setState($text)
	{
		$this->state = (string) $text;
		return $this;
	}

	public function getState()
	{
		return $this->state;
	}
	
	public function setMetroCity($text)
	{
		$this->metrocity = (string) $text;
		return $this;
	}

	public function getMetroCity()
	{
		return $this->metrocity;
	}

	public function setMetroState($text)
	{
		$this->metrostate = (string) $text;
		return $this;
	}

	public function getMetroState()
	{
		return $this->metrostate;
	}

	public function setPostalCode($text)
	{
		$this->postalCode = (string) $text;
		return $this;
	}

	public function getPostalCode()
	{
		return $this->postalCode;
	}
	
	public function setUpdated($text)
	{
		$this->updated = (string) $text;
		return $this;
	}

	public function getUpdated()
	{
		return $this->updated;
	}

	public function setResume($text)
	{
		$this->resume = (string) $text;
		return $this;
	}

	public function getResume()
	{
		return $this->resume;
	}
}

