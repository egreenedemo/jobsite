<?php

class Application_Model_Discipline
{
	protected $discipline;
	protected $disciplineID;

	public function __construct(array $options = null)
	{
		if (is_array($options)) {
			$this->setOptions($options);
		}
	}

	public function __set($name, $value)
	{
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Discipline property');
		}
		$this->$method($value);
	}

	public function __get($name)
	{
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw new Exception('Invalid Discipline property');
		}
		return $this->$method();
	}

	public function setOptions(array $options)
	{
		$methods = get_class_methods($this);
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	public function setDiscipline($text) {
		$this->discipline  = (string)$text;
		return $this;
	}

	public function getDiscipline() {
		return $this->discipline;
	}
	
	public function setDisciplineID($text) {
		$this->disciplineID  = (string)$text;
		return $this;
	}
	
	public function getDisciplineID() {
		return $this->disciplineID;
	}
}

