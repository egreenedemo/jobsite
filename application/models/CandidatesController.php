<?php

class CandidatesController extends Zend_Controller_Action
{
	protected $session;
	
	public function preDispatch()
	{
		/* Initialize action controller here */
		$this->session  = new Zend_Session_Namespace();

	}

	public function indexAction()
	{
		// setup default parameters
		$start = 0;
		$disciplineID = -1;
		$state = "";
		$endrow = 25;
		$total = 0;
		$pageCount = 0;
		$page = 1;

		$requestDisciplineID = $this->_request->getQuery('disciplineID');
		$requestPage = $this->_request->getQuery('page');
		$requestState  = $this->_request->getQuery('state');
		
		if(isset($requestDisciplineID) && $requestDisciplineID > 0) {
			$disciplineID = $requestDisciplineID;
		}

		if(isset($requestPage) && $requestPage > 0) {
			$page = $requestPage;
		}

		if(isset($requestState) && strlen($requestState) > 0) {
			$state = $requestState;
		}

		// Get all Disciplines
		$Disciplines = new Application_Model_DisciplineMapper();
		$this->view->Disciplines = $Disciplines->fetchAll();
		
		if(strlen($state) == 0) {
			$Candidates = new Application_Model_CandidatesMapper();
			$this->view->Candidates = $Candidates->findByDisclipline($disciplineID,$start);			
		}

		if(strlen($state) > 0) {
			$Candidates = new Application_Model_CandidatesMapper();
			$this->view->Candidates = $Candidates->findByState($state,$start);			
		}
		
		$totalrows= $Candidates->fetchCount($disciplineID);

		$this->view->States =$Candidates->getCandidateStates();

		if($totalrows < 25) {
			$endrow = count($this->view->Candidates);
		}

		$paginator = Zend_Paginator::factory($this->view->Candidates);
		$paginator->setCurrentPageNumber($this->_getParam('page'));
		$paginator->setItemCountPerPage(25);
		$paginator->setPageRange(10);
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator.phtml');
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('recordcount.phtml');

		$pagecount = ceil($totalrows / 25);

		$this->view->assign("paginator",$paginator);
		$this->view->assign("disciplineID",$disciplineID);
		$this->view->assign("pagecount",$pagecount);
		$this->view->assign("startrow",$start+1);
		$this->view->assign("endrow",$endrow);
		$this->view->assign("totalrows",$totalrows);
		
		$paginator->setView($this->view);
		// print_r(Zend_Registry::get("connectDB"));
	}

	public function viewAction() {
		$candidateID = $this->_request->getQuery('candidateID');
		
		$candidates = new Application_Model_CandidatesMapper();
		$viewCandidate = $candidates->findByCandidateIDs($candidateID);
		
		$this->view->assign("Candidate",$viewCandidate);
	}
	
	public function searchAction() {
		
		
	}
	
	public function savedAction() {		
		$page = 1;
			
		$requestPage = $this->_request->getQuery('page');

		if(isset($requestPage) && $requestPage > 0) {
			$page = $requestPage;
		}
		
		if(!Zend_Auth::getInstance()->hasIdentity()) {
			$this->_redirect("/auth");
		} 
		
		if(isset($this->session->savedCandidates)) {
			$this->view->savedCandidates = $this->session->savedCandidates;
		} else {
			$this->view->savedCandidates =  array();
		}
			
		// Get all Disciplines
		$Disciplines = new Application_Model_DisciplineMapper();
		$this->view->Disciplines = $Disciplines->getCandidateDisciplines();
		
		$candidates = new Application_Model_CandidatesMapper();
		$this->view->Candidates = $candidates->findByCandidateIDs(implode(",",array_unique(array_filter($this->view->savedCandidates,"strlen"))));
		
		$this->view->States =$candidates->getCandidateStates();
				
		$paginator = Zend_Paginator::factory($this->view->Candidates);
		$paginator->setCurrentPageNumber($this->_getParam('page'));
		$paginator->setItemCountPerPage(25);
		$paginator->setPageRange(10);
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator.phtml');
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('recordcount.phtml');

		//$pagecount = ceil($totalrows / 25);
		
		$paginator->setView($this->view);
		
		$this->view->assign("paginator",$paginator);
	}
	
	public function loginAction() {
			
	}
	
	public function saveAction() {
		$this->_helper->viewRenderer->setNoRender();
		
		$candidateID = $this->_request->getQuery('candidateID');
		$savedCandidates = &$this->session->savedCandidates;
		
		if(!array_search($candidateID, $savedCandidates)) {
			array_push($savedCandidates,$candidateID);		
		}
		
					print_r($this->session->savedCandidates);
			exit;
	}
}