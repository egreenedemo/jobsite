<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
       // $this->_redirect('/login/');
       // $baseURL = new Zend_View_Helper_BaseUrl();
       // $url = $baseURL->baseUrl();
       // $this->view->assign('url', $url);
       $peakJobsSession = new Zend_Session_Namespace("peakJobs");
       $peakJobsSession->loggedIn = 0;
    }

    public function indexAction()
    {	
    	//$cacheUsed = Zend_Registry::get('cacheUsed');
		//$pageCache = Zend_Cache_Frontend_Output::getInstance();
		
	//	$cacheLoaded = false;
		
		//if($cacheUsed == 0 || $cacheLoaded = $appCache->load("indexDisciplines") === false) {
			  // Get all Disciplines
	          $Disciplines = new Application_Model_DisciplineMapper();
	          $this->view->Disciplines = $Disciplines->fetchAll();
	//	}
	$Candidates = new Application_Model_CandidatesMapper();
	$this->view->States =$Candidates->getCandidateStates();
	
	//$this->view->Cities =$Candidates->getCandidateCities();
	
      //  $this->view->assign("cacheUsed",$cacheUsed);
	//	$this->view->assign("appCache",$appCache);

    }
}

