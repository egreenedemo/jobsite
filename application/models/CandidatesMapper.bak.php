<?php

class Application_Model_CandidatesMapper
{
	protected $_dbTable;

	public function setDbTable($dbTable)
	{
		if (is_string($dbTable)) {
			$dbTable = new $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw new Exception('Invalid table data gateway provided');
		}
		$this->_dbTable = $dbTable;
		return $this;
	}

	public function getDbTable()
	{
		if (null === $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Candidates');
		}
		return $this->_dbTable;
	}

	public function save(Application_Model_Candidate $candidate)
	{
		$data = array(
        	'candidate' => $candidate->getCandidate(),
		);

		if (null === ($candidateID = $candidate->getCandidateID())) {
			unset($data['candidateID']);
			$this->getDbTable()->insert($data);
		} else {
			$this->getDbTable()->update($data, array('candidateID = ?' => $candidateID));
		}
	}

	/* find - search for candidate by candidateID */
	public function find($candidateID, Application_Model_Candidate $candidate)
	{
		$result = $this->getDbTable()->find($candidateID);

		if (0 == count($result)) {
			return;
		}
		$row = $result->current();
		$candidate->setCandidateID($row->candidateID)
				->setCity($row->city)
				->setDisciplineID($row->disciplineID)
				->setFirstName($row->firstName)
				->setResume($row->resume)
				->setState($row->state)
				->setUpdated($row->updated);

		return $candidate;
	}

	/* fetchAll - Get all of the candidates in the database */
	public function fetchAll()
	{
		$resultSet = $this->getDbTable()->fetchAll();
		$candidates   = array();
		
		if (0 == count($resultSet)) {
            return;
        }
        
        $row = $resultSet->current();
        
		foreach ($resultSet as $row) {
			$candidate = new Application_Model_Candidates();
			$candidate->setCandidateID($row->candidateID)
				->setCity($row->city)
				->setDisciplineID($row->disciplineID)
				->setFirstName($row->firstName)
				->setResume($row->resume)
				->setState($row->state)
				->setUpdated($row->updated);
			$candidates[] = $candidate;
		}
		return $candidates;
	}

	/* fetchCount - Get the count of all the candidates */
	public function fetchCount($disciplineID) {
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
		$appCache = Zend_Registry::get('appCache');
		$cacheLifetime = Zend_Registry::get('cacheLifetime');
		
		$cacheLoaded = false;
		$totalCount = 0;
		
		try {
			if($disciplineID > 0) {
				$disciplineIDKey = $disciplineID;
			} else {
				$disciplineIDKey = "null";
			}
			if(isset($appCache)) {
				$resultSet = $appCache->load("fetchCount_".preg_replace("/[\W]+/","",$disciplineIDKey));
				
				if(is_array($resultSet) && count($resultSet) > 0) {
					$cacheLoaded = true;
				}
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}
		
		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) {
				$sql = "SELECT
								count(candidates.candidateID) as totalcount
								FROM
								candidates ";
		
				if($disciplineID != -1) {
					$sql .= "WHERE
								candidates.disciplineID = ?";
					$resultSet = $db->query($sql,array($disciplineID));
				} else {
					$resultSet = $db->query($sql);
				}
			}
			
			foreach ($resultSet as $row) {
				if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) {
					if(isset($appCache)) {
						$appCache->save($row,"fetchCount_".preg_replace("/[\W]+/","",$disciplineIDKey),array(),$cacheLifetime);		
					}
				}
				$totalCount = $row['totalcount'];
			}
				
			return $totalCount;
	}

/* find - search for candidate by a variable of parameters */
	public function findAll($disciplineID=0,$state="",$city="",$postalCode="",$start=0)
	{
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
	 	$appCache = Zend_Registry::get('appCache');
	 	$cacheLifetime = Zend_Registry::get('cacheLifetime');
	 			
		$dataCached = false;
		$cacheLoaded = false;
		$resultSet = null;
		$disciplineIDKey = 0;
		
		$candidates   = array();
		
		$sqlparams = array();
		$orderparams = array();
		$fieldparams = "";
		
		if($disciplineID > 0) {
			array_push($sqlparams, $disciplineID);
			array_push($orderparams, "discipline");
		}
		
		if(strlen($state) > 0) {
			if(strcasecmp($city, "Washington") == 0) {
				$state = "DC-MD-VA";
			}
			array_push($sqlparams, $state . '%');
			
			if(strlen($city) ==  0) {
				array_push($sqlparams, $state . '%');
			}
			array_push($orderparams,"candidates.state");
		}
		
		if(strlen($city) > 0) {
			array_push($sqlparams, '%' . $city . '%');
			array_push($orderparams,"candidates.city");
		} 
		
		if(strlen($postalCode) > 0) {
			array_push($sqlparams, $postalCode);
			array_push($orderparams,"candidates.postalCode");
		}
		
		try {
			if($disciplineID > 0) {
				$disciplineIDKey = $disciplineID;
			} else {
				$disciplineIDKey = "null";
			}
			if(isset($appCache)) {
				$candidates = $appCache->load("findAll_disciplineID_".preg_replace("/[\W]+/","",$disciplineIDKey)."_state_0".preg_replace("/[\W]+/","",$state)."_city_0".preg_replace("/[\W]+/","",$city));
				if(is_array($candidates) && count($candidates) > 0) {
					$cacheLoaded = true;
				}	
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}

		if(count($candidates) == 0 || $cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) {

		$sql = "SELECT
						candidates.candidateID,
						discipline.discipline,
						candidates.firstName,
						candidates.city,
						candidates.state,
						candidates.updated,
						provincestate.state as statename,
						SUBSTRING_INDEX(SUBSTRING_INDEX(msaCodes.MsaName,',',1),'-',1) as metrocity,
						TRIM(SUBSTRING(msaCodes.MsaName,INSTR(msaCodes.MsaName,',')+1,3)) as metrostate
						FROM
						candidates
						INNER JOIN discipline ON candidates.disciplineID = discipline.disciplineID
						INNER JOIN zipcodes  ON zipcodes.postalCode = candidates.postalCode
						INNER JOIN provincestate ON provincestate.abbreviation = candidates.state
						INNER JOIN msaCodes ON msaCodes.msaCode = zipcodes.msaCode
						WHERE 1=1";
	
		for($i=0; $i< count($orderparams);$i++) {
			$sql .= " AND ";
			switch($orderparams[$i]) {
				case "discipline": 
					$sql .= " discipline.disciplineID = ?";
					break;
				case "candidates.state": 
					if(strlen($city) > 0) {
						$sql .= " TRIM(MID(msaName,INSTR(msaName,',')+1,LENGTH(msaName))) LIKE ?";
					} else {
						$sql .= " provincestate.state = ? OR candidates.state = ?";
					}
					break;
				case "candidates.city": 
					$sql .= " TRIM(LEFT(msaName,INSTR(msaName,',')-1)) LIKE ?";
					break;
				case "candidates.postalCode": 
					$sql .= " candidates.postalCode = ?";
					break;
			}
		}
		
		$sql .= " ORDER BY candidates.updated DESC";
		
		for($i=0; $i< count($orderparams);$i++) {
			$sql .= ", " . $orderparams[$i];
		}
		/*if($disciplineID != -1) {
			$sql .= " AND
						discipline.disciplineID = ?
						ORDER BY
						discipline, candidates.updated DESC";
			$resultSet = $db->query($sql,array($disciplineID));
		} else {
			$sql .= " ORDER BY
						 ";
			$resultSet = $db->query($sql);
		}
		*/
	    //print_r($sql);
	   //var_dump($sqlparams);
	    
		$resultSet = $db->query($sql,$sqlparams);
	}
				
		if($resultSet != null) {
			foreach ($resultSet as $row) {
				$candidate = new Application_Model_Candidates();
				$candidate->setCandidateID($row['candidateID'])
				->setMetroCity($row['metrocity'])
				->setMetroState($row['metrostate'])
				->setCity($row['city'])
				->setState($row['state'])
				->setFirstName($row['firstName'])
				->setDisciplineID($row['discipline'])
				->setUpdated($row['updated']);
				$candidates[] = $candidate;
			}
			if(isset($appCache)) {
				$appCache->save($candidates,"findAll_disciplineID_".preg_replace("/[\W]+/","",$disciplineIDKey)."_state_0".preg_replace("/[\W]+/","",$state)."_city_0".preg_replace("/[\W]+/","",$city),array(),$cacheLifetime);	
			}
		}
		
		return $candidates;
	}
	
	/* find - search for candidate by disciplineID */
	public function findByDisclipline($disciplineID,$start=0)
	{
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
	 	$appCache = Zend_Registry::get('appCache');
	 	$cacheLifetime = Zend_Registry::get('cacheLifetime');
	 			
		$dataCached = false;
		$cacheLoaded = false;
		$resultSet = null;
		$candidates   = array();
		$disciplineIDKey = 0;
		
		
		try {
			if($disciplineID > 0) {
				$disciplineIDKey = $disciplineID;
			} else {
				$disciplineIDKey = "null";
			}
			
			if(isset($appCache)) {
				$candidates = $appCache->load("findByDiscipline_".preg_replace("/[\W]+/","",$disciplineIDKey));
			
				if(is_array($candidates) && count($candidates) > 0) {
					$cacheLoaded = true;
				}				
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}
		
		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) {
			
			$sql = "SELECT
							candidates.candidateID,
							discipline.discipline,
							candidates.firstName,
							candidates.city,
							candidates.state,
							candidates.updated
							FROM
							discipline
							INNER JOIN candidates ON candidates.disciplineID = discipline.disciplineID";
	
			if($disciplineID != -1) {
				$sql .= " WHERE
							discipline.disciplineID = ?
							ORDER BY
							discipline, candidates.updated DESC";
				$resultSet = $db->query($sql,array($disciplineID));
			} else {
				$sql .= " ORDER BY
							candidates.updated DESC ";
				$resultSet = $db->query($sql);
			}
		}

		//$resultSet = $db->query($sql,array($disciplineID));

		if($resultSet != null) {
			foreach ($resultSet as $row) {
				$candidate = new Application_Model_Candidates();
				$candidate->setCandidateID($row['candidateID'])
				->setCity($row['city'])
				->setState($row['state'])
				->setFirstName($row['firstName'])
				->setDisciplineID($row['discipline'])
				->setUpdated($row['updated']);
				$candidates[] = $candidate;
			}
			
			if(isset($appCache)) {
				$appCache->save($candidates,"findByDiscipline_".preg_replace("/[\W]+/","",$disciplineIDKey),array(),$cacheLifetime);	
			}
		}
		return $candidates;
	}
	
	public function findByState($state, $start=0) {
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
	 	$appCache = Zend_Registry::get('appCache');
	 	$cacheLifetime = Zend_Registry::get('cacheLifetime');
	 			
		$dataCached = false;
		$cacheLoaded = false;
		$resultSet = null;
		$candidates   = array();
		
		try {
			if(isset($appCache)) {
				$candidates = $appCache->load("findByState_".preg_replace("/[\W]+/","",$state));
				if(is_array($candidates) && count($candidates) > 0) {
					$cacheLoaded = true;
				}	
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}

		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) {
				
			$sql = "SELECT SQL_CACHE
							candidates.candidateID,
							discipline.discipline,
							candidates.firstName,
							candidates.city,
							candidates.state,
							candidates.updated,
							SUBSTRING_INDEX(SUBSTRING_INDEX(msaCodes.MsaName,',',1),'-',1) as metrocity,
							TRIM(SUBSTRING(msaCodes.MsaName,INSTR(msaCodes.MsaName,',')+1,3)) as metrostate
							FROM
							discipline
							INNER JOIN candidates ON candidates.disciplineID = discipline.disciplineID
							INNER JOIN provincestate  ON provincestate.abbreviation = candidates.state
							INNER JOIN zipcodes  ON zipcodes.postalCode = candidates.postalCode
							INNER JOIN msaCodes ON msaCodes.msaCode = zipcodes.msaCode";
			
			if(strlen($state)) {
					$sql .= " WHERE
								provincestate.state = ?
								or candidates.state = ?
								ORDER BY
								discipline, candidates.updated DESC";
					$resultSet = $db->query($sql,array($state,$state));
				} else {
					$sql .= " ORDER BY
								candidates.updated DESC ";
					$resultSet = $db->query($sql);
				}
		}

		//print_r($sql);
		
		if($resultSet != null) {
			foreach ($resultSet as $row) {
				$candidate = new Application_Model_Candidates();
				$candidate->setCandidateID($row['candidateID'])
				->setCity($row['city'])
				->setState($row['state'])
				->setMetroCity($row['metrocity'])
				->setMetroState($row['metrostate'])
				->setFirstName($row['firstName'])
				->setDisciplineID($row['discipline'])
				->setUpdated($row['updated']);
			$candidates[] = $candidate;
			}
			
			if(isset($appCache)) {
				$appCache->save($candidates,"findByState_0".preg_replace("/[\W]+/","",$state),array(),$cacheLifetime);	
			}
			
		}
		return $candidates;
	}
	
public function findByCity($postalCode, $start=0) {
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
	 	$appCache = Zend_Registry::get('appCache');
	 	$cacheLifetime = Zend_Registry::get('cacheLifetime');
	 			
		$dataCached = false;
		$cacheLoaded = false;
		$resultSet = null;
		
		$candidates   = array();
		
		try {
			if(isset($appCache)) {
				$candidates = $appCache->load("findByCity_".preg_replace("/[\W]+/","",$postalCode));
				if(is_array($candidates) && count($candidates) > 0) {
					$cacheLoaded = true;
				}	
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}

		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) {

			$sql = "SELECT
							candidates.candidateID,
							discipline.discipline,
							candidates.firstName,
							candidates.city,
							candidates.state,
							candidates.updated,
							candidates.postalCode
							FROM
							candidates
							INNER JOIN discipline ON candidates.disciplineID = discipline.disciplineID
							INNER JOIN zipcodes  ON zipcodes.postalCode = candidates.postalCode
							INNER JOIN msaCodes ON msaCodes.msaCode = zipcodes.msaCode";
	
			//						INNER JOIN provincestate  ON provincestate.abbreviation = candidates.state
			
						//	FROM zipcodes z
						//INNER JOIN candidates c ON c.postalCode = z.postalCode
						//INNER JOIN msaCodes m ON m.msaCode = z.MSACode
			if(strlen($postalCode)) {
					$sql .= " WHERE
								msaCodes.msaCode = ?
								ORDER BY
								discipline, candidates.updated DESC";
					$resultSet = $db->query($sql,array($postalCode));
				} else {
					$sql .= " ORDER BY
								candidates.updated DESC ";
					$resultSet = $db->query($sql);
				}
		}	

		//print_r($sql);
		
		if($resultSet != null) {
			foreach ($resultSet as $row) {
				$candidate = new Application_Model_Candidates();
				$candidate->setCandidateID($row['candidateID'])
				->setCity($row['city'])
				->setState($row['state'])
				->setFirstName($row['firstName'])
				->setDisciplineID($row['discipline'])
				->setUpdated($row['updated']);
			$candidates[] = $candidate;
			}
			
			if(isset($appCache)) {
				$appCache->save($candidates,"findByCity_".preg_replace("/[\W]+/","",$postalCode),array(),$cacheLifetime);	
			}
		}
		return $candidates;
	}
	
	/* getCandidateStates */
	public function getCandidateStates() {
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
	 	$appCache = Zend_Registry::get('appCache');
	 	$cacheLifetime = Zend_Registry::get('cacheLifetime');
	 			
		$dataCached = false;
		$cacheLoaded = false;
		$resultSet = null;

		$states = array();
				
		try {
			if(isset($appCache)) {
				$states = $appCache->load("indexStates");
				if(is_array($states) && count($states) > 0) {
					$cacheLoaded = true;
				}	
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}
		
		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) { //!is_array($disciplines) || count($disciplines) == 0
			$sql = "SELECT SQL_CACHE DISTINCT s.state 
						FROM `candidates` c
						    INNER JOIN `provincestate` s ON s.abbreviation = c.state
						ORDER BY s.state";
		
			$resultSet = $db->query($sql);
		}
		
		if($resultSet != null) {
			foreach($resultSet as $row) {
				$candidate = new Application_Model_Candidates();
				$candidate->setState($row['state']);
				$states[] = $candidate;
			}
			
			if(isset($appCache)) {
				$appCache->save($states,"indexStates",array(),$cacheLifetime);	
			}
		}
		return $states;
	}
	
	public function getCandidateMajorCities() {
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
	 	$appCache = Zend_Registry::get('appCache');
	 	$cacheLifetime = Zend_Registry::get('cacheLifetime');
	 			
		$dataCached = false;
		$cacheLoaded = false;
		$resultSet = null;

		$cities = array();
				
		try {
			if(isset($appCache)) {
				$cities = $appCache->load("indexCities");
				if(is_array($cities) && count($cities) > 0) {
					$cacheLoaded = true;
				}	
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}
		
		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) { 
			$sql = "SELECT 
						m.msaCode,
					     c.postalCode,
						 TRIM(SUBSTRING(m.MsaName,INSTR(m.MsaName,',')+1,3)) as state,
						CASE SUBSTRING_INDEX(m.MsaName,',',1)
							WHEN 'Washington' THEN 'Washington, D.C.'
					    ELSE SUBSTRING_INDEX(SUBSTRING_INDEX(m.MsaName,',',1),'-',1)
						END city
					FROM zipcodes z
						INNER JOIN candidates c ON c.postalCode = z.postalCode
						INNER JOIN msaCodes m ON m.msaCode = z.MSACode
					GROUP BY m.MsaName
					HAVING COUNT(m.msaCode) > 300
					ORDER BY m.MsaName
					LIMIT 0, 35";
			
			$resultSet = $db->query($sql);
		}
		
		if($resultSet != null) {
			foreach($resultSet as $row) {
				$candidate = new Application_Model_Candidates();
				$candidate->setCity($row['city'])
				->setState($row['state'])
				->setPostalCode($row['msaCode']);
				$cities[] = $candidate;
			}
			
			if(isset($appCache)) {
				$appCache->save($cities,"indexCities",array(),$cacheLifetime);	
			}
		}
		return $cities;
	}

	/* getCandidateCities */
	public function getCandidateCities($state) {
		$db = Zend_Registry::get("connectDB");
		$cacheUsed = Zend_Registry::get('cacheUsed');
	 	$appCache = Zend_Registry::get('appCache');
	 	$cacheLifetime = Zend_Registry::get('cacheLifetime');
	 			
		$dataCached = false;
		$cacheLoaded = false;
		$resultSet = null;

		$cities = array();
		
		try {
			if(isset($appCache)) {
				$cities = $appCache->load("getCandidateCities_".preg_replace("/[\W]+/","",$state));
				if(is_array($cities) && count($cities) > 0) {
					$cacheLoaded = true;
				}	
			}
		} catch (Exception $e) {
			echo 'Connection exception';
		}
		
		if($cacheUsed == 0 || ($cacheUsed == 1 && !$cacheLoaded)) { 	
			
			$sql = "SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(m.MsaName,',',1),'-',1) as city
						FROM zipcodes z
							INNER JOIN msaCodes m ON m.msaCode = z.MSACode
							INNER JOIN candidates c ON c.postalCode = z.postalCode
						WHERE z.ProvinceName = '" . str_replace("-"," ",$state) . "' OR  z.ProvinceAbb = '" . str_replace("-"," ",$state) . "'
						GROUP BY m.msaName
						ORDER BY m.msaName";
			$resultSet = $db->query($sql);
		}	

		if($resultSet != null) {
			foreach($resultSet as $row) {
				$candidate = new Application_Model_Candidates();
				$candidate->setCity($row['city']);
				$cities[] = $candidate;
			}
			
			if(isset($appCache)) {
				$appCache->save($cities,"getCandidateCities_".preg_replace("/[\W]+/","",$state),array(),$cacheLifetime);	
			}
		}
		
		return $cities;
	}
	
	/* find - search for candidate by candidateID */
	public function findByCandidateIDs($candidateID)
	{
		$cacheUsed = Zend_Registry::get('cacheUsed');
	 	$appCache = Zend_Registry::get('appCache');
	 	$cacheLifetime = Zend_Registry::get('cacheLifetime');
	 			
		$dataCached = false;
		$resultSet = false;
		$candidates   = array();
			
		if($cacheUsed == 1) {
			try {
				if(isset($appCache)) {
					$resultSet = $appCache->load('candidateID_'.preg_replace("/[\W]+/","",$candidateID));
					$dataCached = true;
				}	
			} catch (Exception $e) {
				// do nothing
			}
		}
		
	 	if($cacheUsed == 0 || $resultSet === false) {			

			$db = Zend_Registry::get("connectDB");
			$candidateArray = explode(",", $candidateID);
			
			$sql = "SELECT
							candidates.candidateID,
							discipline.discipline,
							candidates.disciplineID,
							candidates.firstName,
							candidates.city,
							candidates.state,
							candidates.updated,
							candidates.resume
							FROM
							candidates
							INNER JOIN discipline ON candidates.disciplineID = discipline.disciplineID";
	
			if($candidateID != -1) {
				$sql .= " WHERE
							candidates.candidateID IN (0";
				
				for($i=0;$i<count($candidateArray);$i++) {
					$sql .= ",?";
				}
				
				$sql .=")";
			
				$resultSet = $db->query($sql,$candidateArray);
			}
	 }
	
		foreach ($resultSet as $row) {
			$candidate = new Application_Model_Candidates();
			$candidate->setCandidateID($row['candidateID'])
			->setCity($row['city'])
			->setState($row['state'])
			->setFirstName($row['firstName'])
			->setDisciplineID($row['disciplineID'])
			->setDiscipline($row['discipline'])
			->setUpdated($row['updated'])
			->setResume($row['resume']);
			$candidates[] = $candidate;
			
			if($cacheUsed == 1 && !$dataCached) {
				try {
					if(isset($appCache)) {
						$appCache->save($candidate,'candidateID_'.preg_replace("/[\W]+/","",$candidateID));	
					}
				} catch (Exception $e) {
					//print_r($e);
				}
		}
	}
		return $candidates;
	}
	
	public function swapCandidateStates($state) {
		static $abbreviationArray = array("AL","AK","AR","AZ","CA","CO","CT","DE","FL","GA","HI","IA","ID","IL","IN","KS","KY","LA","MA","MD","ME","MI","MN","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","NY","OH","OK","OR","PA","RI","SC","SD","TN","TX","UT","VA","VT","WA","WI","WV","WY");
		static $nameArray = array("Alabama","Alaska","Arkansas","Arizona","California","Colorado","Connecticut","Delaware","Florida","Georgia","Hawaii","Iowa","Idaho","Illinois","Indiana","Kansas","Kentucky","Louisiana","Massachusetts","Maryland","Maine","Michigan","Minnesota","Missouri","Mississippi","Montana","North Carolina","North Dakota","Nebraska","New Hampshire","New Jersey","New Mexico","Nevada","New York","Ohio","Oklahoma","Oregon","Pennsylvania","Rhode Island","South Carolina","South Dakota","Tennessee","Texas","Utah","Virginia","Vermont","Washington","Wisconsin","West Virginia","Wyoming");
		$returnedState = "";
		
		if(strlen($state) == 2) {
			for($i=0;$i<count($abbreviationArray);$i++) {
				if(strcasecmp($abbreviationArray[$i], trim($state)) == 0) {
					$returnedState = $nameArray[$i];
				}	
			}
		} else {
			for($i=0;$i<count($nameArray);$i++) {
				if(strcasecmp($nameArray[$i],trim($state)) == 0) {
					$returnedState = $abbreviationArray[$i];
				}	
			}
		}
		
		return $returnedState;	
	}
}

