<?php 

class ThankYouController extends Zend_Controller_Action
{
	public function init() {
		
	}
	
	public function indexAction() {
		//$this->_helper->viewRenderer->setNoRender();
		
		// make SMTP default transport for sending mail
		$mailSettings = Zend_Registry::get('mailSettings');
		
		if($mailSettings->useAuthentication ==1) {
			$smtpConfig = array('ssl' => 'tls', 'port' => 587, 'auth' => 'login',
	                'username' =>$mailSettings->username,
	                'password' => $mailSettings->password);
			$transportType = new Zend_Mail_Transport_Smtp($mailSettings->host, $smtpConfig);
		} else {
			$transportType = new Zend_Mail_Transport_Smtp($mailSettings->host);
		}
		
		Zend_Mail::setDefaultTransport($transportType);

		// prepare mail object
		$mailObj = new Zend_Mail();
		
		// get request and loop through form fields
		$request = $this->getRequest();

		// read file contents
		$templateContent = file_get_contents("./data/templates/email_thankyou.html",false);
		
		if ($request->isPost()) {
			$formFields = $request->getParams();
			
			// replace template tags with user entered content
			foreach($formFields as $key=>$value) {
				$templateContent = str_replace("{".$key."}", $value,$templateContent);
			}
			
			$mailObj->setBodyHtml($templateContent);
			$mailObj->setFrom($mailSettings->sender);
			$mailObj->setSubject($mailSettings->thankyousubject);
			$mailObj->addTo($mailSettings->thankyourecipient);
			$mailObj->send();

		}
	}
}
?>