<?php

class CandidatesController extends Zend_Controller_Action
{
	protected $session;
	
	public function preDispatch()
	{
		/* Initialize action controller here */
		$this->session  = new Zend_Session_Namespace();

	}

	public function indexAction()
	{
		// setup default parameters
		$start = 0;
		$disciplineID = -1;
		$state = "";
		$stateabbr = "";
		$city = "";
		$postalCode= "";
		$discipline = "";
		$endrow = 99999999;
		$total = 0;
		$pageCount = 0;
		$page = 1;
		$rowsperpage = 25;
		$multipleParameters = false;
		$paginator = null;
		
		//$requestDisciplineID = $this->_request->getQuery('disciplineID');
		$requestPage = $this->_request->getQuery('page');
		//$requestState  = $this->_request->getQuery('state');
		//$requestCity = $this->_request->getQuery('city');
		
		$urlParams = Zend_Controller_Front::getInstance()->getRequest()->getParams();
		$searchFilters = array("city"=>"","discipline"=>"","disciplineID"=>"","state"=>"","postalCode"=>"");
		
		$urlParamsFound = array_intersect_key($urlParams, $searchFilters);
		$urlParamsFoundKeys = array_keys($urlParamsFound);
				
		/* bug fix - discipline being populated with state - until root cause can be found */
		$i=0;
		foreach($urlParamsFound as $param) {
			$keyName = $urlParamsFoundKeys[$i];
			
			if($keyName  == "discipline" && preg_match("/(alabama|alaska|arizona|arkansas|california|colorado|connecticut|delaware|florida|georgia|hawaii|idaho|illinois|indiana|iowa|kansas|kentucky|louisiana|maine|maryland|massachusetts|michigan|minnesota|mississippi|missouri|montana|nebraska|nevada|new-hampshire|new-jersey|new-mexico|new-york|north-carolina|north-dakota|ohio|oklahoma|oregon|pennsylvania|rhode-island|south-carolina|south-dakota|tennessee|texas|utah|vermont|virginia|washington|west-virginia|wisconsin|wyoming)/", $param)) {
				$urlParamsFound[$keyName] = "null";
				break;
			}
			$i++;
		}

		
		if(count($urlParamsFound) > 1) {
			$paramsCount = 0;
			foreach($urlParamsFound as $param) {
				if($param != "null") {
					$paramsCount++;
				}
			}
			if($paramsCount > 1) {
				$multipleParameters = true;
			}
		}
		/* end bug fix */
		if(!empty($urlParamsFound["disciplineID"])) {
			$requestDisciplineID =$urlParamsFound["disciplineID"];
		}
	
		// get discipline ID from name
		$Disciplines = new Application_Model_DisciplineMapper();
		$candidateDisciplines =$Disciplines->getCandidateDisciplines();
			
		if(!empty($urlParamsFound["discipline"])) {
			// convert string disciplineID to numeric disciplineID
			$requestDisciplineID =str_replace("-engineers","",$urlParamsFound["discipline"]);
						
			foreach ($candidateDisciplines as $thisDiscipline) {
				if(strcasecmp($thisDiscipline->discipline, $requestDisciplineID) == 0) {
					$requestDisciplineID = $thisDiscipline->disciplineID;
					 break;
				}
			}
		}

		if(!empty($urlParamsFound["state"])) {
			$requestState = str_replace("-"," ",$urlParamsFound["state"]);
		}
		
		if(!empty($urlParamsFound["city"])) {
			$requestCity = str_replace("-"," ",$urlParamsFound["city"]);
		}
		
		if(!empty($urlParamsFound["postalCode"])) {
			$requestPostalCode =$urlParamsFound["postalCode"];
		}
		
		if(isset($requestDisciplineID) && $requestDisciplineID > 0) {
			$disciplineID = $requestDisciplineID;
		} else if (!empty($urlParamsFound["disciplineID"])) {
			$disciplineID = $urlParamsFound["disciplineID"];
		}

		if(isset($requestPage) && $requestPage > 0) {
			$page = $requestPage;
		}

		if(isset($requestState) && strlen($requestState) > 0) {
			$state = $requestState;
		} else if (!empty($urlParamsFound["state"])) {
			$state = $urlParamsFound["state"];
		}

		if(isset($requestCity) && strlen($requestCity) > 0) {
			$city = $requestCity;
		} else if (!empty($urlParamsFound["city"])) {
			$city = $urlParamsFound["city"];
		}
		
		if(isset($requestPostalCode) && strlen($requestPostalCode) > 0) {
			$postalCode = $requestPostalCode;
		}	
		
		// Get all Disciplines
		$Disciplines = new Application_Model_DisciplineMapper();
		$this->view->Disciplines = $candidateDisciplines; //$Disciplines->getCandidateDisciplines();
		
		if($multipleParameters  && strcmp($discipline,$state) != 0) { // && strcmp($discipline,$state) != 0
			$Candidates = new Application_Model_CandidatesMapper();
			$this->view->Candidates = $Candidates->findAll($disciplineID,$state,$city,$postalCode,$start);

			if(strlen($state) == 2) {
				$state =$Candidates->swapCandidateStates($state);
			}
		
		} else {
				if(intval($disciplineID) > 0) {
					$Candidates = new Application_Model_CandidatesMapper();
					$this->view->Candidates = $Candidates->findByDisclipline($disciplineID,$start);	
				}
				if(strlen($state) > 0) {
					$Candidates = new Application_Model_CandidatesMapper();
					$this->view->Candidates = $Candidates->findByState($state,$start);		
					
					if(strlen($state) == 2) {
						$state =$Candidates->swapCandidateStates($state);
					}
				}
				
				if(strlen($city) > 0 || strlen($postalCode) > 0) {
					$Candidates = new Application_Model_CandidatesMapper();
					$this->view->Candidates = $Candidates->findByCity($postalCode,$start);			
				}		
				
		}

		if(strlen($disciplineID) > 0) {	
			if($disciplineID != -1 && count($this->view->Candidates)>0) {
				if(is_object($this->view->Candidates[0])) {
					$discipline = $this->view->Candidates[0]->getDisciplineID();	
				}
			}
			
			if(strlen($discipline)==0) {
				$params = $this->_request->getParams();
				if(array_key_exists("discipline", $params)) {
					$discipline = ucwords(str_replace("-engineers", "", $params['discipline']));	
				}
			}
		}
		
				
		if(strlen($state) > 0) {			
			if(is_array($this->view->Candidates) && count($this->view->Candidates)>0) {
				$stateabbr = $this->view->Candidates[0]->getState();	
			}
		}
		
		// Get All States
		$this->view->States =$Candidates->getCandidateStates();
		
		// Get All Cities
		$this->view->Cities = $Candidates->getCandidateCities($state);

		$totalrows= $Candidates->fetchCount($disciplineID);

		if($totalrows < $rowsperpage) {
			$endrow = count($this->view->Candidates);
		}

		if(is_array($this->view->Candidates)) {
			$paginator = Zend_Paginator::factory($this->view->Candidates);
			$paginator->setCurrentPageNumber($this->_getParam('page'));
			$paginator->setItemCountPerPage($rowsperpage);
			$paginator->setPageRange(375/$rowsperpage);
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator.phtml');
			Zend_View_Helper_PaginationControl::setDefaultViewPartial('recordcount.phtml');
		}
		

		$pagecount = ceil($totalrows / $rowsperpage);
		
		$this->view->assign("paginator",$paginator);
		$this->view->assign("disciplineID",$disciplineID);
		$this->view->assign("discipline",$discipline);
		$this->view->assign("pagecount",$pagecount);
		$this->view->assign("startrow",$start+1);
		$this->view->assign("endrow",$endrow);
		$this->view->assign("totalrows",$totalrows);
		$this->view->assign("state",$state);
		$this->view->assign('stateabbr',$stateabbr);
		$this->view->assign("city",$city);
		$this->view->assign("postalCode",$postalCode);
		$this->view->assign("isIndex",false);
		//$this->view->assign("urlParamsFound",$urlParamsFound);
		
		if($paginator != null) {
			$paginator->setView($this->view);
		}
		// print_r(Zend_Registry::get("connectDB"));
		
	}

	public function viewAction() {
		$candidateID = $this->_request->getQuery('candidateID');
		
		$candidates = new Application_Model_CandidatesMapper();
		$viewCandidate = $candidates->findByCandidateIDs($candidateID);
		
		$this->view->assign("Candidate",$viewCandidate);
	}
	
	public function searchAction() {
		
	}
	
	public function savedAction() {		
		$page = 1;
		$rowsperpage = 25;
					
		$requestPage = $this->_request->getQuery('page');

		if(isset($requestPage) && $requestPage > 0) {
			$page = $requestPage;
		}
		
		if(!Zend_Auth::getInstance()->hasIdentity()) {
			$this->_redirect("/auth");
		} 
		
		if(isset($this->session->savedCandidates)) {
			$this->view->savedCandidates = $this->session->savedCandidates;
		} else {
			$this->view->savedCandidates =  array();
		}
			
		// Get all Disciplines
		$Disciplines = new Application_Model_DisciplineMapper();
		$this->view->Disciplines = $Disciplines->getCandidateDisciplines();
		
		$candidates = new Application_Model_CandidatesMapper();
		$this->view->Candidates = $candidates->findByCandidateIDs(implode(",",array_unique(array_filter($this->view->savedCandidates,"strlen"))));
		
		$this->view->States =$candidates->getCandidateStates();
				
		$paginator = Zend_Paginator::factory($this->view->Candidates);
		$paginator->setCurrentPageNumber($this->_getParam('page'));
		$paginator->setItemCountPerPage($rowsperpage);
		$paginator->setPageRange($rowsperpage);
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('paginator.phtml');
		Zend_View_Helper_PaginationControl::setDefaultViewPartial('recordcount.phtml');

		//$pagecount = ceil($totalrows / 25);
		
		$paginator->setView($this->view);
		
		$this->view->assign("paginator",$paginator);
		$this->view->assign("isIndex",false);
	}
	
	public function loginAction() {
			
	}
	
	public function saveAction() {
		$this->_helper->viewRenderer->setNoRender();
		
		$candidateID = $this->_request->getQuery('candidateID');
		$savedCandidates = &$this->session->savedCandidates;
		
		if(!array_search($candidateID, $savedCandidates)) {
			array_push($savedCandidates,$candidateID);		
		}
	}
}