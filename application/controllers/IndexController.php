<?php
      
class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
       // $this->_redirect('/login/');
       // $baseURL = new Zend_View_Helper_BaseUrl();
       // $url = $baseURL->baseUrl();
       // $this->view->assign('url', $url);
       //$peakJobsSession = new Zend_Session_Namespace("peakJobs");
      // $peakJobsSession->loggedIn = 0;

    }

    public function indexAction()
    {			
		//$pageCache = Zend_Cache_Frontend_Output::getInstance();

		// Get all Disciplines
	    $Disciplines = new Application_Model_DisciplineMapper();
	    $this->view->Disciplines = $Disciplines->getIndexDisciplines();
	
	
		$Candidates = new Application_Model_CandidatesMapper();
		$this->view->States =$Candidates->getCandidateStates();
	
	$this->view->Cities =$Candidates->getCandidateMajorCities();
	
      //  $this->view->assign("cacheUsed",$cacheUsed);
	//	$this->view->assign("appCache",$appCache);
		$this->view->assign("isIndex",true);
    }
}

