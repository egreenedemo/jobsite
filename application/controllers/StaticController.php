<?php

class StaticController extends Zend_Controller_Action
{
    
    public function contactusAction() {
    	// Look at /public/contactus.html for static content area
    }
    
    public function privacyAction() {
    	// Look at /public/privacy.html for static content area
    }
}

