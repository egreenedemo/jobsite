<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	protected function _initDoctype() {
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->doctype('HTML5');
	}

	/*protected function _initAutoload() {
		$modelLoader = new Zend_Application_Module_Autoloader(array(
                    'namespace' => 'Application',
                    'basePath' => APPLICATION_PATH
		));

		return $modelLoader;
	}

	protected function _initSession() {
		$dbOption = $this->getOption('resources');
		$dbOption = $dbOption['db'];

		try {
			$db = Zend_Db::factory('Pdo_Mysql', array(
	            'host'        =>$dbOption['params']['host'],
	            'username'    => $dbOption['params']['username'],
	            'password'    =>$dbOption['params']['password'],
	            'dbname'    => $dbOption['params']['dbname'],
			    'dsn' =>$dbOption['params']['dsn']
			));
			Zend_Db_Table_Abstract::setDefaultAdapter($db);
	
			$sessionConfig = array(
	            'name'           => 'session',      
	            'primary'        => 'id',   
	            'modifiedColumn' => 'modified',     
	            'dataColumn'     => 'data',
	            'lifetimeColumn' => 'lifetime'
	            );
	            $saveHandler = new Zend_Session_SaveHandler_DbTable($sessionConfig);
	            Zend_Session::setSaveHandler($saveHandler);
	           
	            Zend_Session::start();
	
	            $pjsession = new Zend_Session_Namespace('peakJobs');
	            $pjsession->loggedOn = 0;
		} catch (Exception $e) {
			
		}
            return $pjsession;
	}
*/
	protected function _initDb() {

		$dbOption = $this->getOption('resources');
		$dbOption = $dbOption['db'];

			// Setup database
			$db = Zend_Db::factory('Pdo_Mysql', array(
	            'host'        =>$dbOption['params']['host'],
	            'username'    => $dbOption['params']['username'],
	            'password'    =>$dbOption['params']['password'],
	            'dbname'    => $dbOption['params']['dbname'],
			    'dsn' 		=> $dbOption['params']['dsn']));
	
			$db->setFetchMode(Zend_Db::FETCH_ASSOC);
			// $db->query("SET NAMES 'utf8'");
			// $db->query("SET CHARACTER SET 'utf8'");
	
			Zend_Registry::set('connectDB', $db);
	
			Zend_Db_Table::setDefaultAdapter($db);
	

		// Return it, so that it can be stored by the bootstrap
		return $db;
	}

	protected function _initRequest() {
		/*	Zend_Session::start();
		$pjsession = new Zend_Session_Namespace('peakJobs');
	
		if(!isset($pjsession->loggedOn)) {
			$pjsession->loggedOn = 0;
		}
		
		return $pjsession;
		*/
	}
	
	protected function _initCache() {
		$cacheOptions = $this->getOption('cache');
		$appCache = null;
		
		if($cacheOptions['useCache'] == 1) {
			$frontendOptions = array(
				'lifetime' => $cacheOptions['lifetime'], // defautls to 1 hour
				'automatic_serialization' => true
			);
	
			$cacheServers = $cacheOptions['cacheServers'];
		

			$backendOptions = array(
				'servers'=>$cacheServers, 'compression'=>false, 'compatibility'=>true // an array of options from config.ini
			);
		
			if(strcasecmp(APPLICATION_ENV,'production') == 0) {
				$cacheBackend = "Libmemcached";
			} else{
				$cacheBackend = "MemCached";
			}
			
			$appCache = Zend_Cache::factory('Core',$cacheBackend,$frontendOptions, $backendOptions);
		}
		
		Zend_Registry::set('cacheUsed',$cacheOptions['useCache']);
		Zend_Registry::set('appCache',$appCache);
		Zend_Registry::set('cacheLifetime',$cacheOptions['lifetime']);
		
		return $appCache;
	}
	
	protected function _initRouter() {
		$ctrl  = Zend_Controller_Front::getInstance();
		$ctrl->setParam('useDefaultControllerAlways', true);
		$router = $ctrl->getRouter(); // returns a rewrite router by default

		 $route = new Zend_Controller_Router_Route_Regex(
		    'engineers/?$',
		    array(
		        'controller' => 'index',
		        'action'     => 'index'
		    ),
   			 'engineers/'
		);
		
		$router->addRoute('index', $route);
		
		$route = new Zend_Controller_Router_Route_Regex(
		    'engineers/thankyou?$',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'thankyou'
		    ),
   			 'engineers/thankyou'
		);
		
		$router->addRoute('thankyou', $route);
		
		 $route = new Zend_Controller_Router_Route_Regex(
		    'engineers/(?!engineers)([al]{3}\-{1}[engirs]{9})(?>/page/([0-9]*))?$',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
   			 'engineers/all-engineers/page/%s'
		);
		
		$router->addRoute('allengineers', $route);
		
	     $route = new Zend_Controller_Router_Route_Regex(
		    'engineers/(?!engineers)(alabama|alaska|arizona|arkansas|california|colorado|connecticut|delaware|florida|georgia|hawaii|idaho|illinois|indiana|iowa|kansas|kentucky|louisiana|maine|maryland|massachusetts|michigan|minnesota|mississippi|missouri|montana|nebraska|nevada|new-hampshire|new-jersey|new-mexico|new-york|north-carolina|north-dakota|ohio|oklahoma|oregon|pennsylvania|rhode-island|south-carolina|south-dakota|tennessee|texas|utah|vermont|virginia|washington|west-virginia|wisconsin|wyoming|puerto-rico|district-of-columbia)(?>/page/([0-9]*))?',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
		    array(
		        1 => 'state',
		        2=>'page'
		    ),
   			 'engineers/%s/page/%s'
		);
		
		$router->addRoute('states', $route);
		
		$route = new Zend_Controller_Router_Route_Regex(
		    'engineers/(?!engineers)([a-zA-Z\-]+\-{1}[engirs]{9})(?>/page/([0-9]*))?$',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
		    array(
		        1 => 'discipline',
		        2=>'page'
		    ),
   			 'engineers/%s/page/%s'
		);

		$router->addRoute('disciplines', $route);
			
		$route = new Zend_Controller_Router_Route_Regex(
		    'engineers/([a-z]{2})\/{1}([a-zA-Z\s\-\.]+)(?>/page/([0-9]*))?',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
		    array(
		        1 => 'state',
		        2 => 'city',
		        3=>'page'
		    ),
   			 'engineers/%s/%s/page/%s'
		);
		
		$router->addRoute('cities', $route);
	
		$route = new Zend_Controller_Router_Route_Regex(
		    'engineers/(?!engineers)([a-zA-Z\-]+\-{1}[engirs]{9})/(alabama|alaska|arizona|arkansas|california|colorado|connecticut|delaware|florida|georgia|hawaii|idaho|illinois|indiana|iowa|kansas|kentucky|louisiana|maine|maryland|massachusetts|michigan|minnesota|mississippi|missouri|montana|nebraska|nevada|new-hampshire|new-jersey|new-mexico|new-york|north-carolina|north-dakota|ohio|oklahoma|oregon|pennsylvania|rhode-island|south-carolina|south-dakota|tennessee|texas|utah|vermont|virginia|washington|west-virginia|wisconsin|wyoming|puerto-rico|district-of-columbia)(?>/page/([0-9]*))?$',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
		    array(
		       	1=> 'discipline',
		        2 => 'state',
		        3=>'page'
		    ),
   			 'engineers/%s/%s/page/%s'
		);

		$router->addRoute('disciplinestates', $route);
		
		$route = new Zend_Controller_Router_Route_Regex(
		    'engineers/(?!engineers)([a-zA-Z\-]+)/([a-zA-Z]+)/([a-zA-Z\s\-\.]+)[\/]*(?>/page/([0-9]*))?$',
		    array(
		        'controller' => 'candidates',
		        'action'     => 'index'
		    ),
		    array(
		       	1=> 'discipline',
		        2 => 'state',
		        3 => 'city',
		        4=>'page'
		    ),
   			 'engineers/%s/%s/%s/page/%s'
		);
		
		$router->addRoute('disciplinecities', $route);
		return $router;
		
	}
	
	protected function _initConfig()
	{
		$config = new Zend_Config($this->getOptions());
		Zend_Registry::set('config', $config);
		return $config;
	}	
	
	protected function _initMail()
	{
		$mail = new Zend_Config($this->getOption('mail'));
		Zend_Registry::set('mailSettings', $mail);
		return $mail;
	}	
	
	protected function _initErrorHandling()
	{
		$errorSettings = new Zend_Config($this->getOption('error'));
		Zend_Registry::set('errorSettings', $errorSettings);
		return $errorSettings;
	}	
	
	protected function _initViewHelpers() {
    	$view = new Zend_View();
    	$view->headTitle('PEAK Technical Staffing USA')->setSeparator(' | ');
	}

}

