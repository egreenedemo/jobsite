<?php

class LoginController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        // action body
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('nomenu');
        
        $loginForm = new  Application_Form_Login();
        $this->view->loginForm = $loginForm;
    }
}

